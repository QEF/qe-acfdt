#!/bin/sh

# run from directory where this script is
cd `echo $0 | sed 's/\(.*\)\/.*/\1/'` # extract pathname
EXAMPLE_DIR=`pwd`

# check whether ECHO has the -e option
if test "`echo -e`" = "-e" ; then ECHO=echo ; else ECHO="echo -e" ; fi

$ECHO
$ECHO "This example shows how to use pw.x and acfdt.x to"
$ECHO "compute the Random Phase Approximation (RPA) correlation energy via "
$ECHO "Adiabatic Connection Fluctuation Dissipation (ACFD)."
$ECHO "This example is for Be atom/molecule."

# set the needed environment variables
. ../../environment_variables

# required executables and pseudopotentials
BIN_LIST="pw.x acfdt.x exx.x"
PSEUDO_LIST="Be.EXX-fhi.UPF"

$ECHO
$ECHO "  executables directory: $BIN_DIR"
$ECHO "  pseudo directory:      $PSEUDO_DIR"
$ECHO "  temporary directory:   $TMP_DIR"
$ECHO
$ECHO "  checking that needed directories and files exist...\c"


for DIR in "$TMP_DIR" "$EXAMPLE_DIR/example01" ; do
    if test ! -d $DIR ; then
        mkdir $DIR
    fi
done
cd $EXAMPLE_DIR/example01

# check for directories
for DIR in "$BIN_DIR" "$PSEUDO_DIR" ; do
    if test ! -d $DIR ; then
        $ECHO
        $ECHO "ERROR: $DIR not existent or not a directory"
        $ECHO "Aborting"
        exit 1
    fi
done
 
# check for pseudopotentials
for FILE in $PSEUDO_LIST ; do
    if test ! -r $PSEUDO_DIR/$FILE ; then
        $ECHO
        $ECHO "ERROR: $PSEUDO_DIR/$FILE not existent or not readable"
        $ECHO "Aborting"
        exit 1
    fi
done
$ECHO " done"

# check for executables
for FILE in $BIN_LIST ; do
    if test ! -x $BIN_DIR/$FILE ; then
        $ECHO
        $ECHO "ERROR: $BIN_DIR/$FILE not existent or not executable"
        $ECHO "Aborting"
        exit 1
    fi
done

# how to run executables
PW_COMMAND="$PARA_PREFIX $BIN_DIR/pw.x $PARA_POSTFIX"
ACFDT_COMMAND="$PARA_PREFIX $BIN_DIR/acfdt.x $PARA_POSTFIX"
EXX_COMMAND="$PARA_PREFIX $BIN_DIR/exx.x $PARA_POSTFIX"
$ECHO
$ECHO "  running pw.x as:      $PW_COMMAND"
$ECHO "  running acfdt.x as:   $ACFDT_COMMAND"
$ECHO

# clean TMP_DIR
$ECHO "  cleaning $TMP_DIR ..."
read -p "  ARE YOU SURE??? Press any key to continue ..."
rm -rf $TMP_DIR/*
$ECHO "  DONE"

# self-consistent calculation for Be atom/molecule

cat > Be.scf.in << EOF
&CONTROL
  calculation = "scf",
  pseudo_dir  = "$PSEUDO_DIR",
  outdir      = "$TMP_DIR",
  prefix='Be'
  lecrpa = .true.,
  tstress = .false.,
  tprnfor = .false.,
/
&SYSTEM
  ibrav=  1,
  celldm(1) = 10,
  nat=  1, 
  ntyp= 1,
  ecutwfc = 10,
  nbnd = 2
/
&ELECTRONS
  diagonalization='david'
  mixing_mode = 'plain'
  mixing_beta = 0.7
  conv_thr =  0.5d-12
/
ATOMIC_SPECIES
  Be 1.00 Be.EXX-fhi.UPF
ATOMIC_POSITIONS {angstrom}
  Be 0.00 0.00 0.00 
K_POINTS {automatic}
  1 1 1  0 0 0
EOF
$ECHO " running the scf calculation for Beryllium ...\c"
$PW_COMMAND < Be.scf.in > Be.scf.out
$ECHO " done"

# post-processing for potential

cat > Be.acfdt.in << EOF
EC-RPA of Be 
 &inputec
  tr2_ph=1.0d-15,
  prefix='Be'
  outdir='$TMP_DIR',
  oep_method = 0
  idiag = 3,
  iumax = 10,
  nf_ec = 10,
  xqq(1)= 0.00, xqq(2)= 0.00, xqq(3)= 0.00, wq=1.00
  u0_ec = 30,
  neigv = 20,  
 /
EOF
$ECHO "  running acfdt.x calculation for Beryllium ...\c"
$ACFDT_COMMAND < Be.acfdt.in > Be.acfdt.out


# post-processing for potential

cat > Be.exx.in << EOF
EXX of Be
 &inputexx
  prefix = 'Be'
  outdir = '$TMP_DIR',
  nqx1   = 1,
  nqx2   = 1,
  nqx3   = 1,
  exx_fraction = 1.d0
  exxdiv_treatment      = "gygi-baldereschi"
  x_gamma_extrapolation = .FALSE.
 /
EOF
$ECHO "  running exx.x calculation for Beryllium ...\c"
$EXX_COMMAND < Be.exx.in > Be.exx.out

