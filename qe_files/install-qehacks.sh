#!/bin/bash
#
# files in install
#
list=""
for file in ${list}; do
   filedest="../../install/${file}"
   filedest_bkp="../../install/${file}.bkp"
   cmp ${file} ${filedest} 1> /dev/null
   if [ $? -eq 0 ]; then
      echo "${filedest} is not modified"
   else
      echo "${filedest} is replaced by the new one"
      if [ ! -e ${filedest_bkp} ] ; then /bin/cp -f ${filedest} ${filedest_bkp}; fi
      /bin/cp -f ${file} ${filedest}
   fi
done
#
# files in Modules
#
list="xml_io_base.f90"
for file in ${list}; do
   filedest="../../Modules/${file}"
   filedest_bkp="../../Modules/${file}.bkp"
   cmp ${file} ${filedest} 1> /dev/null
   if [ $? -eq 0 ]; then
      echo "${filedest} is not modified"
   else
      echo "${filedest} is replaced by the new one"
      if [ ! -e ${filedest_bkp} ] ; then /bin/cp -f ${filedest} ${filedest_bkp}; fi
      /bin/cp -f ${file} ${filedest}
   fi
done
#
# files in PW/src
#
#list="non_scf.f90 setup.f90 punch.f90 exx.f90"
list="non_scf.f90 setup.f90 exx.f90"
for file in ${list}; do
   filedest="../../PW/src/${file}"
   filedest_bkp="../../PW/src/${file}.bkp"
   cmp ${file} ${filedest} 1> /dev/null
   if [ $? -eq 0 ]; then
      echo "${filedest} is not modified"
   else
      echo "${filedest} is replaced by the new one"
      if [ ! -e ${filedest_bkp} ] ; then /bin/cp -f ${filedest} ${filedest_bkp}; fi
      /bin/cp -f ${file} ${filedest}
   fi
done
#
# files in PHonon/PH
#
list="phq_setup.f90 check_initial_status.f90"
for file in ${list}; do
   filedest="../../PHonon/PH/${file}"
   filedest_bkp="../../PHonon/PH/${file}.bkp"
   cmp ${file} ${filedest} 1> /dev/null
   if [ $? -eq 0 ]; then
      echo "${filedest} is not modified"
   else
      echo "${filedest} is replaced by the new one"
      if [ ! -e ${filedest_bkp} ] ; then /bin/cp -f ${filedest} ${filedest_bkp}; fi
      /bin/cp -f ${file} ${filedest}
   fi
done
#
echo ""
