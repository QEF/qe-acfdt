!
! Copyright (C) 2008 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!
!-----------------------------------------------------------------------
SUBROUTINE psi_2_bare(dvdpsi, evc, ikk, ikq, dvpsi, dpsi)
!------------------------------------------------------------------------
  !
  ! This routine to calculate rhs of linear eqs of |psi_2>.
  ! |dvdpsi_v> = |dvdpsi_v> - S|dpsi_v'><psi_v'|(DV)*|psi_v>  (1)
  ! This routine does: 
  !        1) ps = <psi_v'|dvpsi_v>
  !        2) calc. S|dpsi_v'>
  !        3) apply (1) to find |dvdpsi>
  ! In general, this routine works like routine orthogonalize ()
  !
  ! NB: INPUT is dvpsi and dpsi ; 
  !   : dpsi_1 is used as work_space of S|dpsi> calculation
  !   : OUTPUT is dvdpsi
  !
  USE kinds,              ONLY : DP
  USE klist,              ONLY : lgauss, degauss, ngauss, ngk
  USE noncollin_module,   ONLY : noncolin, npol
  USE wvfct,              ONLY : npwx, nbnd, et
  USE ener,               ONLY : ef
  USE control_lr,         ONLY : alpha_pv, nbnd_occ
  USE becmod,             ONLY : becp, calbec
  USE uspp,               ONLY : vkb, okvan
  USE mp_global,          ONLY : intra_pool_comm
  USE mp,                 ONLY : mp_sum
  !
  !
  IMPLICIT NONE
  INTEGER, INTENT(IN) :: ikk, ikq   ! the index of the k and k+q points
  COMPLEX(DP), INTENT(IN) :: evc(npwx*npol,nbnd)
  COMPLEX(DP), INTENT(IN) :: dvpsi(npwx*npol,nbnd)
  COMPLEX(DP), INTENT(INOUT) :: dvdpsi(npwx*npol,nbnd)
  COMPLEX(DP), INTENT(IN) :: dpsi  (npwx*npol,nbnd)
  COMPLEX(DP), ALLOCATABLE :: dpsi_1(:,:) ! work space allocated by
                                                     ! the calling routine
  
  COMPLEX(DP), ALLOCATABLE :: ps(:,:)
  INTEGER :: ibnd, jbnd, nbnd_eff
  REAL(DP) :: wg1, w0g, wgp, wwg, deltae, theta
  REAL(DP), EXTERNAL :: w0gauss, wgauss
  INTEGER :: npwq
  ! functions computing the delta and theta function
  
  CALL start_clock ('psi_2_bare')
  ALLOCATE(ps(nbnd,nbnd))
  ALLOCATE(dpsi_1(npwx*npol,nbnd))
  !
  npwq = ngk(ikq) ! NsC: Need to be defined it's no more global variable
  if (lgauss) then
     !
     !  metallic case
     !
     ps = (0.d0, 0.d0)
     IF (noncolin) THEN
        CALL zgemm( 'C', 'N', nbnd, nbnd_occ (ikk), npwx*npol, (1.d0,0.d0), &
                   evc, npwx*npol, dvpsi, npwx*npol, (0.d0,0.d0), ps, nbnd )
     ELSE
        CALL zgemm( 'C', 'N', nbnd, nbnd_occ (ikk), npwq, (1.d0,0.d0), &
                   evc, npwx, dvpsi, npwx, (0.d0,0.d0), ps, nbnd )
     END IF
     !
     DO ibnd = 1, nbnd_occ (ikk)
        wg1 = wgauss ((ef-et(ibnd,ikk)) / degauss, ngauss)
        w0g = w0gauss((ef-et(ibnd,ikk)) / degauss, ngauss) / degauss
        DO jbnd = 1, nbnd
           wgp = wgauss ( (ef - et (jbnd, ikq) ) / degauss, ngauss)
           deltae = et (jbnd, ikq) - et (ibnd, ikk)
           theta = wgauss (deltae / degauss, 0)
           wwg = wg1 * (1.d0 - theta) + wgp * theta
           IF (jbnd <= nbnd_occ (ikq) ) THEN
              IF (abs (deltae) > 1.0d-5) THEN
                  wwg = wwg + alpha_pv * theta * (wgp - wg1) / deltae
              ELSE
                 !
                 !  if the two energies are too close takes the limit
                 !  of the 0/0 ratio
                 !
                 wwg = wwg - alpha_pv * theta * w0g
              ENDIF
           ENDIF
           !
           ps(jbnd,ibnd) = wwg * ps(jbnd,ibnd)
           !
        ENDDO
        IF (noncolin) THEN
           CALL dscal (2*npwx*npol, wg1, dvpsi(1,ibnd), 1)
        ELSE
           call dscal (2*npwq, wg1, dvpsi(1,ibnd), 1)
        END IF
     END DO
     nbnd_eff=nbnd
  ELSE
     !
     !  insulators
     !
     ps = (0.d0, 0.d0)
     IF (noncolin) THEN
        CALL zgemm( 'C', 'N',nbnd_occ(ikq), nbnd_occ(ikk), npwx*npol, &
               (1.d0,0.d0), evc, npwx*npol, dvpsi, npwx*npol, &
               (0.d0,0.d0), ps, nbnd )
     ELSE
  !      CALL zgemm( 'C', 'N', nbnd_occ(ikq), nbnd_occ (ikk), npwq, &
  !             (1.d0,0.d0), evc, npwx, dvpsi, npwx, &
  !             (0.d0,0.d0), ps, nbnd )
        CALL zgemm( 'C', 'N', nbnd_occ(ikk), nbnd_occ (ikk), npwx, &
               (1.d0,0.d0), evc, npwx, dvpsi, npwx, &
               (0.d0,0.d0), ps, nbnd )
     END IF
     nbnd_eff=nbnd_occ(ikk)
  END IF
#if defined __MPI
     call mp_sum(ps(:,1:nbnd_eff),intra_pool_comm)
#endif
  !
  ! dpsi_1 is used as work space to store S|dpsi>
  !
  !IF (okvan) CALL calbec ( npwq, vkb, evq, becp, nbnd_eff)
  ! notsure right or wrong
  !
  CALL s_psi (npwx, npwq, nbnd_eff, dpsi, dpsi_1)
  !
  ! |dvdpsi> =  (|dvdpsi> - S|dpsi><evc|dvpsi>)
  !
  if (lgauss) then
     !
     !  metallic case
     !
     IF (noncolin) THEN
        CALL zgemm( 'N', 'N', npwx*npol, nbnd_occ(ikk), nbnd, &
                  (-1.d0,0.d0), dpsi_1, npwx*npol, ps, nbnd, (1.0d0,0.d0), &
                  dvdpsi, npwx*npol )
     ELSE
        CALL zgemm( 'N', 'N', npwq, nbnd_occ(ikk), nbnd, &
               (-1.d0,0.d0), dpsi_1, npwx, ps, nbnd, (1.0d0,0.d0), &
                dvdpsi, npwx )
     END IF
  ELSE
     !
     !  Insulators: note that nbnd_occ(ikk)=nbnd_occ(ikq) in an insulator
     !
     IF (noncolin) THEN
        CALL zgemm( 'N', 'N', npwx*npol, nbnd_occ(ikk), nbnd_occ(ikk), &
                  (-1.d0,0.d0),dpsi_1,npwx*npol,ps,nbnd,(1.0d0,0.d0), &
                  dvdpsi, npwx*npol )
     ELSE
        !CALL zgemm( 'N', 'N', npwq, nbnd_occ(ikk), nbnd_occ(ikk), &
        CALL zgemm( 'N', 'N', npwq, nbnd_occ(ikk), nbnd_occ(ikk), &
               (-1.d0,0.d0), dpsi_1, npwx, ps, nbnd, (1.0d0,0.d0), &
                dvdpsi, npwx )
     END IF
  ENDIF
  
  DEALLOCATE(ps)
  DEALLOCATE(dpsi_1)
  CALL stop_clock ('psi_2_bare')
  !
  RETURN
  END SUBROUTINE psi_2_bare
