!
! Copyright (C) 2001 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!
!-----------------------------------------------------------------------
subroutine solve_linter_iu ( uu )
  !-----------------------------------------------------------------------
  !
  !    Driver routine for the solution of the linear system which
  !    defines the change of the wavefunction due to an external
  !    perturbation with given q-vector. There are several possible
  !    calculations depending on the values of some control variables
  !    1) if lchi0=.true., it computes dn = Xo(iu)|dv> ( scf cycle in not needed )
  !    2) lchi=.true., it computes dn = X_rpa(iu)|dv>  (  scf cycle is needed, but
  !                    xc-potential is set to zero in RPA )
  !    In genereal, it performs the following tasks:
  !     a) computes the bare potential term Delta V | psi >
  !        note that Delta V must be in R-space and stored in dvgen(c)
  !     b) applies P_c^+ (orthogonalization to valence states)
  !     c) calls bicgstabsolve_all to solve the linear system
  !     e) computes Delta rho
  !
  USE kinds,                 ONLY : DP
  USE ions_base,             ONLY : nat
  USE io_global,             ONLY : stdout
  USE wavefunctions_module,  ONLY : evc, psic
  USE klist,                 ONLY : lgauss, nkstot, wk, xk, ngk, igk_k
  USE lsda_mod,              ONLY : lsda, nspin, current_spin, isk
  USE fft_base,              ONLY : dffts, dfftp
  USE fft_interfaces,        ONLY : fwfft, invfft
  USE gvect,                 ONLY : gstart, nl
  USE gvecs,                 ONLY : doublegrid, nls
  USE becmod,                ONLY : becp, calbec
  USE wvfct,                 ONLY : npwx, nbnd, et, current_k
  USE uspp_param,            ONLY : nhm
  USE control_lr,            ONLY : lgamma, nbnd_occ
  USE control_ph,            ONLY : nmix_ph, tr2_ph, lgamma_gamma, convt, &
                                    alpha_mix, ldisp, rec_code 
  USE uspp,                  ONLY : nlcc_any
  USE units_ph,              ONLY : iudwf, lrdwf, iuwfc, lrwfc
  USE eqv,                   ONLY : dvpsi, dpsi, evq
  USE qpoint,                ONLY : nksq, nksq, ikks, ikqs
  USE uspp,                  ONLY : nkb, vkb, okvan  
  USE mp,                    ONLY : mp_sum
  USE mp_global,             ONLY : intra_pool_comm, inter_pool_comm 
  USE control_acfdt,         ONLY : dpsi_plus, dpsi_minus, control_vc_rpa, dvgenc
  USE noncollin_module,      ONLY : noncolin, npol, nspin_mag
  USE paw_variables,         ONLY : okpaw
  USE buffers,               ONLY : get_buffer
  !
  implicit none
  !
  integer, parameter :: npe = 1
  !
  real(DP), intent(in) :: uu 
  ! input: the imaginary frequency
  !
  !complex(DP) :: drhoscf (nrxx, nspin, 1)
  complex(DP), allocatable :: drhoscf (:,:,:)
  complex(DP), allocatable :: h_diag (:,:,:), etiu (:,:,:)
  ! h_diag: diagonal part of the Hamiltonian +/- iu
  ! etiu  : eigenvalues +/- iu
  real(DP) , allocatable :: rh_diag (:,:)
  ! rh_diag: diagonal part of the Hamiltonian
  real(DP) :: thresh, anorm, dr2
  ! thresh: convergence threshold
  ! anorm : the norm of the error
  ! dr2   : self-consistency error
  real(DP) :: dos_ef, weight
  ! dos_ef: DOS at Fermi energy (in calculation for a metal)
  ! weight: weight of k-point     
  complex(DP), allocatable, target :: dvscfin(:,:,:)
  ! change of the scf potential 
  complex(DP), pointer :: dvscfins (:,:,:)
  ! change of the scf potential (smooth part only)
  complex(DP), allocatable :: drhoscfh (:,:,:), dvscfout (:,:,:)
  ! change of rho / scf potential (output)
  ! change of scf potential (output)
  complex(DP), allocatable :: ldos (:,:), ldoss (:,:),&
       dbecsum (:,:,:,:), aux1 (:)
  ! Misc work space
  ! ldos : local density of states af Ef
  ! ldoss: as above, without augmentation charges
  ! dbecsum: the derivative of becsum
  complex(DP), allocatable :: dpsi2(:,:), dvpsi_aux(:,:)
  ! working array for dvpsi and dpsi for (+) and (-) iu
  REAL(DP), allocatable :: becsum1(:,:,:)
  !
  complex(DP) :: ZDOTC
  ! the scalar product function

  logical :: conv_root,  & ! true if linear system is converged
             iuzero,     & ! true if freq uu==0
             exst,       & ! used to open the recover file
             lmetq0        ! true if xq=(0,0,0) in a metal

  integer :: kter,       & ! counter on iterations
             iter0,      & ! starting iteration
             ibnd, jbnd, & ! counter on bands
             iter,       & ! counter on iterations
             lter,       & ! counter on iterations of linear system
             lintercall, & ! average number of calls to cgsolve_all
             ik, ikk,    & ! counter on k points
             ikq,        & ! counter on k+q points
             ig,         & ! counter on G vectors
             ir,         & ! counter on mesh points
             is,         & ! counter on spin polarizations
             ios           ! integer variable for I/O control
  !           mode          ! mode index

  integer :: irr, kilobytes
  integer :: npw, npwq

  real(DP) :: tcpu, get_clock ! timing variables

  external cch_psi_all, ccg_psi
  external ch_psi_all,  cg_psi
  !
  call start_clock ('solve_linter_iu')

  !
  iuzero = abs(uu).lt.1.d-9
  !
  if ( iuzero ) then
     allocate( rh_diag(npwx, nbnd) )
  else
     allocate ( h_diag(npwx, nbnd, 2) )
     allocate ( dpsi2(npwx,nbnd) )
     allocate ( etiu(nbnd, nkstot, 2) )
     etiu(:,:,1) = CMPLX( et(:,:), uu, kind=DP )
     etiu(:,:,2) = CMPLX( et(:,:),-uu, kind=DP )
  endif

  allocate (dvscfin (dfftp%nnr, nspin , npe))    
  if (doublegrid) then
     allocate (dvscfins (dffts%nnr , nspin , npe))    
  else
     dvscfins => dvscfin
  endif
  allocate (drhoscf  (dfftp%nnr, nspin, 1) )
  allocate (drhoscfh (dfftp%nnr, nspin , npe))    
  allocate (dvscfout (dfftp%nnr, nspin , npe))    
  allocate (dbecsum ( (nhm * (nhm + 1))/2 , nat , nspin , npe))    
  allocate (aux1 ( dffts%nnr ))    
  allocate ( dvpsi_aux(npwx,nbnd) )
  !
  ! if q=0 for a metal: allocate and compute local DOS at Ef
  !
  lmetq0 = lgauss.and.lgamma
  if (lmetq0) then
     allocate ( ldos ( dfftp%nnr, nspin) )    
     allocate ( ldoss( dffts%nnr, nspin) )    
     allocate (becsum1 ( (nhm * (nhm + 1))/2 , nat , nspin_mag))
     call localdos_paw ( ldos , ldoss , becsum1, dos_ef )
     IF (.NOT.okpaw) deallocate(becsum1)
  endif
  !
  !   The outside loop is over the iterations
  !   !!!!!!! ACTUALLY NO LOOP HERE !!!!!!!!! 
  !
  do kter = 1, 1 
     iter = kter + iter0
     !
     lintercall = 0
     drhoscf(:,:, 1) = (0.d0, 0.d0)
     dbecsum(:,:,:,:) = (0.d0, 0.d0)
     !
     !
!     if (nksq.gt.1) rewind (unit = iunigk)
     do ik = 1, nksq
!        if (nksq.gt.1) then
!           read (iunigk, err = 100, iostat = ios) npw, igk
!100        call errore ('solve_dviu', 'reading igk', abs (ios) )
!        endif
!        if (lgamma) npwq = npw
        ikk = ikks(ik)
        ikq = ikqs(ik)
        !NsC >
        npw = ngk(ikk)
        npwq= ngk(ikq)
        !
        current_k = ikq ! for RPA Ec with HF or hybrid functionals
        if (lsda) current_spin = isk (ikk)
!        if (.not.lgamma.and.nksq.gt.1) then
!           read (iunigk, err = 200, iostat = ios) npwq, igkq
!200        call errore ('solve_dviu', 'reading igkq', abs (ios) )
!        endif
        !
        ! reads unperturbed wavefuctions psi(k) and psi(k+q)
        !
        if (nksq.gt.1) then
           if (lgamma) then
!              call davcio (evc, lrwfc, iuwfc, ikk, - 1)
              call get_buffer (evc, lrwfc, iuwfc, ikk)  ! NsC from version 283
           else
!              call davcio (evc, lrwfc, iuwfc, ikk, - 1)
              call get_buffer (evc, lrwfc, iuwfc, ikk) ! NsC from version 283
!              call davcio (evq, lrwfc, iuwfc, ikq, - 1)
              call get_buffer (evq, lrwfc, iuwfc, ikq) ! NsC from version 283
           endif
        endif
        !
        ! compute beta functions and kinetic energy for k-point ikq
        ! needed by h_psi, called by ch_psi_all, called by cgsolve_all
        !
        CALL init_us_2 (npwq, igk_k(1,ikq), xk (1, ikq), vkb)
        CALL g2_kin (ikq) 
        ! compute preconditioning matrix h_diag used by cgsolve_all
        !
        IF (iuzero) THEN 
           CALL h_prec (ik, evq, rh_diag)
        ELSE 
           CALL ch_prec (ik, evq, h_diag, uu)
        ENDIF
        !
!        call init_us_2 (npwq, igkq, xk (1, ikq), vkb)
!        ! compute the kinetic energy
!        !
!        do ig = 1, npwq
!           g2kin (ig) = ( (xk (1,ikq) + g (1, igkq(ig)) ) **2 + &
!                          (xk (2,ikq) + g (2, igkq(ig)) ) **2 + &
!                          (xk (3,ikq) + g (3, igkq(ig)) ) **2 ) * tpiba2
!        enddo
!        !
!        !
!        if ( iuzero ) then
!           rh_diag(:,:) = 0.d0
!           do ibnd = 1, nbnd_occ (ikk)
!              do ig = 1, npwq
!                 rh_diag(ig,ibnd) = 1.d0 / max(1.0d0,g2kin(ig)/eprec(ibnd,ik))
!              enddo
!           enddo
!       if (.NOT. iuzero)
!           h_diag(:,:,:) = (0.d0, 0.d0)
!           do ibnd = 1, nbnd_occ (ikk)
!              do ig = 1, npwq
!                  h_diag(ig,ibnd,1) = CMPLX(1.d0, 0.d0, kind=DP) / &
!                      CMPLX( max(1.0d0,g2kin(ig)/eprec(ibnd,ik))-et(ibnd,ik),-uu, kind=DP )
!                  h_diag(ig,ibnd,2) = CMPLX(1.d0, 0.d0, kind=DP) / &
!                      CMPLX( max(1.0d0,g2kin(ig)/eprec(ibnd,ik))-et(ibnd,ik), uu, kind=DP )
!              enddo
!           enddo
!        endif
        !
        ! This is one difference from other version of PDFT
        !
        ! compute the right hand side of the linear system due to
        ! a lattice-periodic perturbation, dvscfin used as work space
        !
        dvpsi(:,:) = (0.d0, 0.d0)
        dvpsi_aux(:,:) = (0.d0, 0.d0)
        dvscfin(:,current_spin,1) = dvgenc(:,current_spin,1)
        if (doublegrid) &
           call cinterpolate (dvscfin(1,current_spin,1),dvscfins(1,current_spin,1),-1)
        !   
        do ibnd = 1, nbnd_occ (ik)
           aux1(:) = (0.d0, 0.d0)
           do ig = 1, npw
!              aux1 (nls(igk(ig)))=evc(ig,ibnd)
              aux1 (nls(igk_k(ig,ikk)))=evc(ig,ibnd)
           enddo
           CALL invfft ('Wave', aux1, dffts)
           do ir = 1, dffts%nnr
               aux1(ir)=aux1(ir)*dvscfins(ir,current_spin,1)
           enddo
           !
           CALL fwfft ('Wave', aux1, dffts)
           do ig = 1, npwq
!              dvpsi(ig,ibnd)=dvpsi(ig,ibnd)+aux1(nls(igkq(ig)))
              dvpsi(ig,ibnd)=dvpsi(ig,ibnd)+aux1(nls(igk_k(ig,ikq)))
           enddo
        enddo
        if (okvan) then
           call errore('solve_dviu', 'USPP not implemented yet', 1)
        endif
        !        
        dvpsi_aux(:,:) = dvpsi(:,:)
        !
        ! iterative solution of the linear system (H-eS)*dpsi=dvpsi,
        ! dvpsi=-P_c^+ (dvbare+dvscf)*psi , dvscf fixed.
        !
        thresh = tr2_ph
        !
        if ( iuzero ) then
           !
           ! Ortogonalize dvpsi to valence states: ps = <evq|dvpsi>
           ! Apply -P_c^+. 
           ! And finally |dvspi> =  -(|dvpsi> - S|evq><evq|dvpsi>)
           !
           dvpsi=dvpsi_aux
           CALL orthogonalize(dvpsi, evq, ikk, ikq, dpsi, npwq, .false.)
           !
           weight = wk (ikk); conv_root = .true.; dpsi(:,:)=(0.d0,0.d0)
           call cgsolve_all (ch_psi_all, cg_psi, et(1,ikk), dvpsi, dpsi, &
                             rh_diag, npwx, npwq, thresh, ik, lter, conv_root, &
                             anorm, nbnd_occ(ikk), 1)
           if (nksq.gt.1) call davcio ( dpsi, lrdwf, iudwf, ik, +1)
           !
           if (control_vc_rpa) then
              dpsi_plus  (:,:) = dpsi (:,:)
              dpsi_minus (:,:) = dpsi (:,:)
           end if
           !
           if (.not.conv_root) WRITE( stdout, '(5x,"kpoint",i4," ibnd",i4,  &
                &              " solve_linter_iu: root not converged ",e10.3)') &
                &              ik , ibnd, anorm
          
        else
           ! +iu
           dvpsi=dvpsi_aux
           CALL orthogonalize_uu(dvpsi, evq, ikk, ikq, dpsi, npwq, uu)
           !
           conv_root = .true.; dpsi(:,:)=(0.d0,0.d0)
           call bicgstabsolve_all (cch_psi_all, ccg_psi, etiu(1,ikk,1), &
                             dvpsi, dpsi, h_diag(1,1,1), npwx, npwq, thresh, ik, &
                             lter, conv_root, anorm, nbnd_occ(ikk) )
           if (.not.conv_root) WRITE( stdout, '(5x,"kpoint",i4," ibnd",i4,  &
                &              " solve_linter_iu: root not converged ",e10.3)') &
                &              ik , ibnd, anorm
           !
           if (control_vc_rpa) then 
              if (nksq.gt.1) then
                 call davcio ( dpsi, lrdwf, iudwf, ik, +1)
              else
                 dpsi_plus (:,:) = dpsi (:,:) 
              end if
           end if 
           !  
           ! -iu
           dvpsi=dvpsi_aux
           CALL orthogonalize_uu(dvpsi, evq, ikk, ikq, dpsi2, npwq, -uu)
           !
           conv_root = .true.; dpsi2(:,:)=(0.d0,0.d0)
           call bicgstabsolve_all (cch_psi_all, ccg_psi, etiu(1,ikk,2), &
                             dvpsi, dpsi2, h_diag(1,1,2), npwx, npwq, thresh, ik, &
                             lter, conv_root, anorm, nbnd_occ(ikk) )
           if (.not.conv_root) WRITE( stdout, '(5x,"kpoint",i4," ibnd",i4,  &
                &              " solve_linter_iu: root not converged ",e10.3)') &
                &              ik , ibnd, anorm
           !
           ! 
           if (control_vc_rpa) then
                if (nksq.gt.1) then  
                   call davcio ( dpsi2, lrdwf, iudwf, ik+nksq, +1)
                else 
                   dpsi_minus (:,:) = dpsi2 (:,:)
                end if
           end if
           ! 
           ! NB: a factor of 2 will be added to weight inside incdrhoscf
           dpsi(:,:) = dpsi(:,:) + dpsi2(:,:); weight = wk(ikk)/2.d0
           !
        endif
        !
        ! calculates drho, sum over k 
        !
        call incdrhoscf (drhoscf(1,current_spin,1), weight, ik, &
                         dbecsum(1,1,current_spin,1), dpsi)
     enddo ! on k-points
     !
     ! 
#if defined __MPI
     !
     !  The calculation of dbecsum is distributed across processors (see addusdbec)
     !  Sum over processors the contributions coming from each slice of bands
     !
     call mp_sum (dbecsum, intra_pool_comm)
     ! 
#endif
     !
     if (doublegrid) then
        do is = 1, nspin
           call cinterpolate (drhoscfh(1,is,1), drhoscf(1,is,1), 1)
        enddo
     else
        call ZCOPY (npe*nspin*dfftp%nnr, drhoscf, 1, drhoscfh, 1)
     endif
     !
     ! if q=0, make sure that charge conservation is guaranteed
     !
     if ( lgamma ) then
        psic(:) = drhoscfh(:, nspin, npe)
        CALL fwfft ('Dense', psic, dfftp)
        !CALL cft3 (psic, nr1, nr2, nr3, nrx1, nrx2, nrx3, -1)
        if ( gstart==2) psic(nl(1)) = (0.d0, 0.d0)
        CALL invfft ('Dense', psic, dfftp)
        !CALL cft3 (psic, nr1, nr2, nr3, nrx1, nrx2, nrx3, +1)
        drhoscfh(:, nspin, npe) = psic(:)
     endif
     !
     !    Now we compute for all perturbations the total charge and potential
     !
     !call addusddens (drhoscfh, dbecsum, irr, imode0, npe, 0)
#if defined __MPI
     !
     !   Reduce the delta rho across pools
     !
     call mp_sum (drhoscf, inter_pool_comm)
     !
     call mp_sum (drhoscfh, inter_pool_comm)
     !
#endif
     !
     ! here we pass drhoscf to dvgenc to used as output
     !
     convt = .true.
     if ( convt ) then
        dvgenc(:,:,2) = drhoscfh(:,:,1) 
        goto 111
     endif
     !
  enddo ! loop over iteration
  ! 
  !
  111 CONTINUE
  !
  !
  if (lmetq0) deallocate (ldoss)
  if (lmetq0) deallocate (ldos)
  if ( iuzero ) then
     deallocate (rh_diag)
  else
     deallocate (h_diag, etiu, dpsi2)
  endif
  deallocate (aux1)
  deallocate (dbecsum)
  deallocate (drhoscf )
  deallocate (dvscfout)
  deallocate (drhoscfh)
  if (doublegrid) deallocate (dvscfins)
  deallocate (dvscfin)
  deallocate ( dvpsi_aux )

  call stop_clock ('solve_linter_iu')
  return
end subroutine solve_linter_iu
