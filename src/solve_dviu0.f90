!
! Copyright (C) 2001 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!
!-----------------------------------------------------------------------
subroutine solve_dviu0 ( )
  !-----------------------------------------------------------------------
  !
  !    Driver routine for the solution of the linear system which
  !    defines the change of the wavefunction due to an external
  !    perturbation with given q-vector. For the time being, it is
  !    used only for diagonalizing the noninteracting Kohn-Sham 
  !    response function,thus self-consistent loop is not needed.
  !
  !    It performs the following tasks:
  !     a) computes the bare potential term Delta V | psi > 
  !        and an additional term in the case of US pseudopotentials
  !     b) applies P_c^+ (orthogonalization to valence states)
  !     c) calls bicgstabsolve_all to solve the linear system
  !     e) computes Delta rho
  !
  USE kinds,                ONLY : DP
  USE fft_base,             ONLY : dffts, dfftp
  USE fft_interfaces,       ONLY : fwfft, invfft
  USE cell_base,            ONLY : omega, tpiba2
  USE ions_base,            ONLY : nat
  USE io_global,            ONLY : stdout, ionode
  USE io_files,             ONLY : diropn 
  USE wavefunctions_module, ONLY : evc
  USE gvect,                ONLY : g
  USE gvecs,                ONLY : nls, doublegrid
  USE wvfct,                ONLY : et, npwx, nbnd
  USE klist,                ONLY : xk, wk, nelec, lgauss, ngk, igk_k
  USE lsda_mod,             ONLY : lsda, isk, current_spin, nspin
  USE noncollin_module,     ONLY : noncolin
  USE uspp_param,           ONLY : nhm
  USE control_lr,           ONLY : lgamma, nbnd_occ
  USE control_ph,           ONLY : reduce_io, tr2_ph
  USE units_ph,             ONLY : iudrho, lrdrho, iuwfc, lrwfc
  USE output,               ONLY : fildrho
  USE eqv,                  ONLY : dvpsi, dpsi, evq
  USE qpoint,               ONLY : nksq, ikks, ikqs
  USE uspp,                 ONLY : vkb, okvan
  USE mp,                   ONLY : mp_sum 
  USE mp_global,            ONLY : intra_pool_comm, inter_pool_comm   
  USE control_acfdt,        ONLY : dvgen, dvgenc
  USE noncollin_module,      ONLY : noncolin, npol, nspin_mag
  USE paw_variables,        ONLY : okpaw
  USE buffers,              ONLY : get_buffer
  !
  implicit none

  integer, parameter :: npe = 1

  complex(DP) :: drhoscf (dfftp%nnr, nspin, 1)
  ! the change of the scf charge
  real(DP) , allocatable :: h_diag (:,:)
  ! h_diag: diagonal part of the Hamiltonian 
  !real(DP) , allocatable :: eprec (:)
  ! eprec : array for preconditioning
  real(DP) :: thresh, anorm, averlt, dr2, nocharge
  ! thresh: convergence threshold
  ! anorm : the norm of the error
  ! averlt: average number of iterations
  ! dr2   : self-consistency error
  real(DP) :: dos_ef, wg1, w0g, wgp, weight, deltae, theta
  complex(DP) :: wwg
       
  ! Misc variables for metals
  ! dos_ef: density of states at Ef
  real(DP), external :: w0gauss, wgauss
  ! functions computing the delta and theta function

  complex(DP), allocatable, target :: dvscfin(:,:,:)
  ! change of the scf potential 
  complex(DP), pointer :: dvscfins (:,:,:)
  ! change of the scf potential (smooth part only)
  complex(DP), allocatable :: drhoscfh (:,:,:), dvscfout (:,:,:)
  ! change of rho / scf potential (output)
  ! change of scf potential (output)
  complex(DP), allocatable :: ldos (:,:), ldoss (:,:),&
       dbecsum (:,:,:,:), auxg (:), aux1 (:), ps (:,:)
  ! Misc work space
  ! ldos : local density of states af Ef
  ! ldoss: as above, without augmentation charges
  ! dbecsum: the derivative of becsum
  complex(DP) :: ZDOTC
  ! the scalar product function
  REAL(DP), allocatable :: becsum1(:,:,:)

  logical :: conv_root,  & ! true if linear system is converged
             exst,       & ! used to open the recover file
             lmetq0        ! true if xq=(0,0,0) in a metal

  integer :: kter,       & ! counter on iterations
             iter0,      & ! starting iteration
             ibnd, jbnd, & ! counter on bands
             iter,       & ! counter on iterations
             lter,       & ! counter on iterations of linear system
             lintercall, & ! average number of calls to cgsolve_all
             ik, ikk,    & ! counter on k points
             ikq,        & ! counter on k+q points
             ig,         & ! counter on G vectors
             ir,         & ! counter on mesh points
             is,         & ! counter on spin polarizations
             nrec, nrec1,& ! the record number for dvpsi and dpsi
             ios,        & ! integer variable for I/O control
             mode          ! mode index

  integer :: irr
  integer :: npw, npwq

  real(DP) :: tcpu, get_clock ! timing variables

  character (len=256) :: flmixdpot ! name of the file with the mixing potential

  external ch_psi_all, cg_psi
  !
  call start_clock ('solve_dviu0')
  allocate (ps (nbnd, nbnd))
  allocate (dvscfin ( dfftp%nnr, nspin , npe))    
  if (doublegrid) then
     allocate (dvscfins ( dffts%nnr, nspin , npe))    
  else
     dvscfins => dvscfin
  endif
  allocate (drhoscfh ( dfftp%nnr, nspin , npe))    
  allocate (dvscfout ( dfftp%nnr, nspin , npe))    
  allocate (auxg (npwx))    
  allocate (dbecsum ( (nhm * (nhm + 1))/2 , nat , nspin , npe))    
  allocate (aux1 ( dffts%nnr))    
  !allocate (eprec ( nbnd))
  allocate (h_diag ( npwx , nbnd))    
  !
  ! if q=0 for a metal: allocate and compute local DOS at Ef
  !
  lmetq0 = lgauss.and.lgamma
  if (lmetq0) then
     allocate ( ldos ( dfftp%nnr , nspin) )    
     allocate ( ldoss( dffts%nnr , nspin) )    
     allocate (becsum1 ( (nhm * (nhm + 1))/2 , nat , nspin_mag))
     call localdos_paw ( ldos , ldoss , becsum1, dos_ef )
     IF (.NOT.okpaw) deallocate(becsum1)
  endif
  !
  if (reduce_io) then
     flmixdpot = ' '
  else
     flmixdpot = 'mixd'
  endif
  !
  IF (ionode .AND. fildrho /= ' ') THEN
     INQUIRE (UNIT = iudrho, OPENED = exst)
     IF (exst) CLOSE (UNIT = iudrho, STATUS='keep')
     CALL DIROPN (iudrho, TRIM(fildrho)//'.u', lrdrho, exst)
  end if
  !
  !   The outside loop is over the iterations
  !   !!!!!!! ACTUALLY NO LOOP HERE !!!!!!!!! 
  !
  do kter = 1, 1 
     iter = kter + iter0


     lintercall = 0
     drhoscf(:,:, 1) = (0.d0, 0.d0)
     dbecsum(:,:,:,:) = (0.d0, 0.d0)
     !
!     if (nksq.gt.1) rewind (unit = iunigk)
     do ik = 1, nksq
!        if (nksq.gt.1) then
!           read (iunigk, err = 100, iostat = ios) npw, igk
!100        call errore ('solve_dviu0', 'reading igk', abs (ios) )
!        endif
        ikk = ikks(ik)
        ikq = ikqs(ik)
        npw = ngk(ikk)
        npwq= ngk(ikq)
        if (lsda) current_spin = isk (ikk)
!        if (.not.lgamma.and.nksq.gt.1) then
!           read (iunigk, err = 200, iostat = ios) npwq, igkq
!200        call errore ('solve_dviu0', 'reading igkq', abs (ios) )
!
!        endif
        !
        ! reads unperturbed wavefuctions psi(k) and psi(k+q)
        !
        if (nksq.gt.1) then
           if (lgamma) then
!              call davcio (evc, lrwfc, iuwfc, ikk, - 1)
              call get_buffer (evc, lrwfc, iuwfc, ikk) ! NsC from version 283
           else
!              call davcio (evc, lrwfc, iuwfc, ikk, - 1)
              call get_buffer (evc, lrwfc, iuwfc, ikk) ! NsC from version 283
!              call davcio (evq, lrwfc, iuwfc, ikq, - 1)
              call get_buffer (evc, lrwfc, iuwfc, ikq) ! NsC from version 283
           endif
        endif
        !
        ! compute beta functions and kinetic energy for k-point ikq
        ! needed by h_psi, called by ch_psi_all, called by cgsolve_all
        !
!        call init_us_2 (npwq, igkq, xk (1, ikq), vkb)
        CALL init_us_2 (npwq, igk_k(1,ikq), xk (1, ikq), vkb)
        CALL g2_kin (ikq) 
        ! compute preconditioning matrix h_diag used by cgsolve_all
        !
        CALL h_prec (ik, evq, h_diag)
        !
!        ! compute the kinetic energy
!        !
!        do ig = 1, npwq
!           g2kin (ig) = ( (xk (1,ikq) + g (1, igkq(ig)) ) **2 + &
!                          (xk (2,ikq) + g (2, igkq(ig)) ) **2 + &
!                          (xk (3,ikq) + g (3, igkq(ig)) ) **2 ) * tpiba2
!        enddo
!        !
!        !
!        h_diag=0.d0
!        do ibnd = 1, nbnd_occ (ikk)
!           do ig = 1, npwq
!               h_diag(ig,ibnd)=1.d0/max(1.0d0,g2kin(ig)/eprec(ibnd,ik))
!               !h_diag(ig,ibnd)=CMPLX(1.d0, 0.d0, kind=DP) / &
!               !   CMPLX( max(1.0d0,g2kin(ig)/eprec(ibnd,ik))-et(ibnd,ik),-uu, kind=DP )
!           enddo
!           IF (noncolin) THEN
!              do ig = 1, npwq
!               h_diag(ig+npwx,ibnd)=1.d0/max(1.0d0,g2kin(ig)/eprec(ibnd,ik))
!               !h_diag(ig+npwx,ibnd)=CMPLX(1.d0, 0.d0, kind=DP) / &
!                  !CMPLX( max(1.0d0,g2kin(ig)/eprec(ibnd,ik))-et(ibnd,ik),-uu, kind=DP )
!              enddo
!           END IF
!        enddo
        ! 
        ! compute the right hand side of the linear system due to
        ! a lattice-periodic perturbation, dvscfin used as work space
        !
        dvpsi(:,:) = (0.d0, 0.d0)
        if (lgamma) then
           dvscfin(:,current_spin,1) = CMPLX(dvgen(:,current_spin), 0.d0, kind=DP)
        else
           dvscfin(:,current_spin,1) = dvgenc(:,current_spin,1)
        endif

        if (doublegrid) & 
           call cinterpolate (dvscfin(1,current_spin,1),dvscfins(1,current_spin,1),-1)

        do ibnd = 1, nbnd_occ (ik)
           aux1(:) = (0.d0, 0.d0)
           do ig = 1, npw
!              aux1 (nls(igk(ig)))=evc(ig,ibnd)
              aux1 (nls(igk_k(ig,ikk)))=evc(ig,ibnd)
           enddo
           !call cft3s (aux1,nr1s,nr2s,nr3s,nrx1s,nrx2s,nrx3s,+2)
           CALL invfft ('Wave', aux1, dffts)
           do ir = 1, dffts%nnr
               aux1(ir)=aux1(ir)*dvscfins(ir,current_spin,1)
           enddo
           !call cft3s (aux1,nr1s,nr2s,nr3s,nrx1s,nrx2s,nrx3s,-2)
           CALL fwfft ('Wave', aux1, dffts)
           do ig = 1, npwq
!              dvpsi(ig,ibnd)=dvpsi(ig,ibnd)+aux1(nls(igkq(ig)))
              dvpsi(ig,ibnd)=dvpsi(ig,ibnd)+aux1(nls(igk_k(ig,ikq)))
           enddo
        enddo

        if (okvan) then
           call errore('solve_dviu0', 'USPP not implemented yet', 1)
           !call newdq(dvscfin(1,current_spin,1), 1)
           !call adddvscf(1,ik)
        endif
        !
        ! Ortogonalize dvpsi to valence states: ps = <evq|dvpsi>
        ! Apply -P_c^+. 
        ! And finally |dvspi> =  -(|dvpsi> - S|evq><evq|dvpsi>)
        !
        CALL orthogonalize(dvpsi, evq, ikk, ikq, dpsi, npwq, .false.)
        !  
        conv_root = .true.;  thresh = tr2_ph
        call cgsolve_all (ch_psi_all, cg_psi, et(1,ikk), &
                          dvpsi, dpsi, h_diag, npwx, npwq, thresh, ik, &
                          lter, conv_root, anorm, nbnd_occ(ikk) )

        if (.not.conv_root) WRITE( stdout, '(5x,"kpoint",i4," ibnd",i4,  &
             &              " solve_dviu0: root not converged ",e10.3)') &
             &              ik , ibnd, anorm
        !
        ! calculates drho, sum over k 
        !
        weight = wk (ikk)
        call incdrhoscf (drhoscf(1,current_spin,1), weight, ik, &
                         dbecsum(1,1,current_spin,1), dpsi)
     enddo ! on k-points
#if defined __MPI
     !
     !  The calculation of dbecsum is distributed across processors (see addusdbec)
     !  Sum over processors the contributions coming from each slice of bands
     !
     !call reduce (nhm * (nhm + 1) * nat * nspin * npe, dbecsum)
      call mp_sum (dbecsum, intra_pool_comm)
     !
#endif

     if (doublegrid) then
        do is = 1, nspin
           call cinterpolate (drhoscfh(1,is,1), drhoscf(1,is,1), 1)
        enddo
     else
        call ZCOPY (npe*nspin*dfftp%nnr, drhoscf, 1, drhoscfh, 1)
     endif
     !
     !    Now we compute for all perturbations the total charge and potential
     !
     !call addusddens (drhoscfh, dbecsum, irr, imode0, npe, 0)
#if defined __MPI
     !
     !   Reduce the delta rho across pools
     !
     !call poolreduce (2 * npe * nspin * nrxx, drhoscf)
     !call poolreduce (2 * npe * nspin * nrxx, drhoscfh)
      call mp_sum (drhoscf, inter_pool_comm)
     !
      call mp_sum (drhoscfh, inter_pool_comm)
     ! 
#endif
     !
     ! q=0 in metallic case deserve special care (e_Fermi can shift)
     !
!     if (lmetq0) call ef_shift(drhoscfh, ldos, ldoss, dos_ef, irr, npe, .false.)
     !
     !   After the loop over the perturbations we have the linear change 
     !   in the charge density for each mode of this representation. 
     !   Here we symmetrize them ...
     !
     !
     ! here we pass drhoscf to dvgen(c) to used as output
     !
     if (lgamma) then
        dvgen(:,:) = REAL(drhoscfh(:,:,1))
        nocharge = SUM(dvgen(:,:)) * omega/(dfftp%nr1*dfftp%nr2*dfftp%nr3) / nelec
     !   CALL reduce( 1, nocharge )
        call mp_sum(nocharge, intra_pool_comm)   
     ! 
        if ( nocharge .gt. 1.0d-3 ) then
           write(stdout, * )nocharge
           call errore('solve_dviu0', 'charge not conserved', 1)
        endif
     else
        ! In the case of calculation at finite q and finite iu, the weight 
        ! for calculation of drho as done in the routine incdrhoscf has been 
        ! multiplied by factor of 2. Here we remove it
        dvgenc(:,:,2) = dvgenc(:,:,2) + drhoscfh(:,:,1) / ( 2.D0, 0.D0 )
     endif
  enddo
!155 iter0=0
  if (lmetq0) deallocate (ldoss)
  if (lmetq0) deallocate (ldos)
  !deallocate (eprec)
  deallocate (h_diag)
  deallocate (aux1)
  deallocate (dbecsum)
  deallocate (auxg)
  deallocate (dvscfout)
  deallocate (drhoscfh)
  if (doublegrid) deallocate (dvscfins)
  deallocate (dvscfin)
  deallocate (ps)

  call stop_clock ('solve_dviu0')
  return
end subroutine solve_dviu0
