
! Copyright (C) 2009 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
#define ZERO ( 0.D0, 0.D0 )
#define ONE  ( 1.D0, 0.D0 )
!
!------------------------------------------------------------------
SUBROUTINE read_write_dec (dchi, dchi12, dchi3, ifor) 
 !
 !
 USE kinds,         ONLY : DP
 USE gvect,         ONLY : ngm, nl
 USE fft_base,      ONLY : dfftp
 USE fft_interfaces,ONLY : fwfft, invfft
 USE control_acfdt, ONLY : iundedv 
   !
   implicit none
   !
   ! I/O variables
   !
   integer, intent(in):: ifor
   ! ifor = -1 read from disk
   ! ifor =  1 write to  disk
   ! 
   complex(dp), intent(inout):: dchi(dfftp%nnr)
   complex(dp), intent(inout):: dchi12(dfftp%nnr)
   complex(dp), intent(inout):: dchi3(dfftp%nnr)
   !
   ! local variables
   !
   complex(dp), allocatable :: aux_pot(:,:)
   !
#if defined alloc_aux
   complex(dp), allocatable :: aux(:)
#else
   complex(dp) :: aux(dfftp%nnr)
#endif
   !
   allocate (aux_pot(ngm,3))

       if (ifor==-1) then
           !
           ! read 3 dec/dv vectors from disk
           !
           call init_depot( 3, aux_pot, -1, iundedv )
           !
           ! FFT to R space
           !
           ! put dvg on the FFT grid
           aux(:) = ZERO; aux(nl(1:ngm)) = aux_pot(1:ngm, 1)
           ! go to R-space
           CALL invfft ('Dense', aux, dfftp)
           ! keep only the real part
           dchi(:) = aux(:)
           !
           ! put dvg on the FFT grid
           aux(:) = ZERO; aux(nl(1:ngm)) = aux_pot(1:ngm, 2)
           ! go to R-space
           CALL invfft ('Dense', aux, dfftp)
           ! keep only the real part
           dchi12(:) = aux(:)
           !  
           ! put dvg on the FFT grid
           aux(:) = ZERO; aux(nl(1:ngm)) = aux_pot(1:ngm, 3)
           ! go to R-space
           CALL invfft ('Dense', aux, dfftp)
           ! keep only the real part
           dchi3(:) = aux(:)
           ! 
       elseif (ifor==1) then
           !
           ! FFT to G space
           !
           ! put dchi's on the FFT grid
           aux(:) = ZERO; aux(:) = dchi(:) 
           ! go back to G-space
           CALL fwfft ('Dense', aux, dfftp)
           ! put back to dvg
           aux_pot(1:ngm, 1) = aux(nl(1:ngm))
           !
           ! put dchi's on the FFT grid
           aux(:) = ZERO; aux(:) = dchi12(:)
           ! go back to G-space
           CALL fwfft ('Dense', aux, dfftp)
           ! put back to dvg
           aux_pot(1:ngm, 2) = aux(nl(1:ngm))
           !
           ! put dchi's on the FFT grid
           aux(:) = ZERO; aux(:) = dchi3(:)
           ! go back to G-space
           CALL fwfft ('Dense', aux, dfftp)
           ! put back to dvg
           aux_pot(1:ngm, 3) = aux(nl(1:ngm))
           ! 
           ! write 3 dec/dv vectors on disk
           ! 
           call init_depot( 3, aux_pot, +1, iundedv )
           ! 
       else
       ! 
       endif
       !
       deallocate (aux_pot)
#if defined alloc_aux
       deallocate (aux)
#endif
       return
       !
  end subroutine
