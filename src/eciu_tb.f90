
! Copyright (C) 2001 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
#define ZERO ( 0.D0, 0.D0 )
#define ONE  ( 1.D0, 0.D0 )
!#define DEBUG
!
!----------------------------------------------------------------------
SUBROUTINE eciu_tb ( iu, uu, nevl, e )
  !--------------------------------------------------------------------
  !
  !	This subroutine calculates contribution to Ec at given (imaginary)
  !     frequency in ACFDT formalism. It computes low-lying eigenvalues of 
  !     the generalized eigenvalue problem 
  !                    (Xo - a*Vc^{-1})|w> = 0
  !     by using tight-binding approach for DBS.
  !     Eigenpotentials for each monomer are stored in different files
  !     (not optimal yet when monomers are the same)
  !
  USE kinds,                ONLY : DP
  USE mp,                   ONLY : mp_bcast
  USE mp_global,            ONLY : intra_pool_comm, intra_image_comm
  USE constants,            ONLY : tpi
  USE cell_base,            ONLY : alat
  USE io_global,            ONLY : stdout, ionode, ionode_id
  USE io_files,             ONLY : tmp_dir, diropn
  USE gvect,                ONLY : ngm, ngm_g, g,  gstart
  USE qpoint,               ONLY : xq
  USE mp,                   ONLY : mp_sum
  USE control_acfdt,        ONLY : lepsi, depdir1, depdir2, pot_collect, iundvw, &
                                   tau1, tau2
  USE control_lr,           ONLY : lgamma
  !
  IMPLICIT NONE
  !
  ! I/O variables
  !
  REAL(DP), INTENT(IN) :: uu 
  ! input: imaginary frequency 
  REAL(DP), INTENT(OUT) :: e(nevl) 
  ! output: eigenvalues
  INTEGER, INTENT(IN)  :: iu, nevl
  ! input: index of frequency
  ! input: number of eigenmodes to be computed
  !
  ! local variables
  !
  CHARACTER(LEN=256), EXTERNAL :: trimcheck
  !
  INTEGER :: half_nevl
  COMPLEX(DP), ALLOCATABLE :: dvg(:,:), dng(:,:), depot1(:,:), depot2(:,:)
  ! potentials and density response in G-space
  COMPLEX(DP), ALLOCATABLE :: chi0(:,:), vc(:,:), xr(:,:)
  REAL(DP), ALLOCATABLE :: ew(:), norm(:)
  ! Xo in reduced basis set
  ! Vc^{-1} in reduced basis set
  ! estimated eigenpotentials 
  ! estimated eigenvalues 
  INTEGER :: m, n, l, nb1, info, ig, i, j
  ! counter on iterations
  ! dimension of the reduced basis
  ! counter on the reduced basis vectors
  ! do-loop counters
  REAL(DP) :: arg, r0(3), epsi00
  ! 
  LOGICAL :: opnd, exst 
  ! counters
  REAL(DP), EXTERNAL :: DDOT
  COMPLEX(DP), EXTERNAL :: ZDOTC
  CHARACTER(LEN=256) :: temp_dir_save, string
  CHARACTER(LEN=6), EXTERNAL :: int_to_char
  !
  WRITE(stdout,'(7x,''Diagonalize Vc^{1/2}*Xo*Vc^{1/2} using linear combination of DEP of monomers'')')
  !
  ! various checkings
  !
  half_nevl = nevl/2
  IF (ngm_g .LT. nevl)  CALL errore('eciu_tb', 'nevl TOO LARGE', 1)
  IF ( mod(nevl,2) /= 0)  CALL errore('eciu_tb', 'nevl must be even', 1)
  !
  ! local arrays  
  !
  ALLOCATE ( dvg(ngm,nevl) ) 
  ALLOCATE ( norm(nevl) ) 
  ALLOCATE ( chi0(nevl,nevl),vc(nevl,nevl), xr(nevl,nevl), ew(nevl) )
  ALLOCATE ( depot1(ngm,half_nevl), depot2(ngm,half_nevl) )
  !
  CALL start_clock ('eciu_tb')
  !
  dvg(:,:) = ZERO; dng(:,:) = ZERO
  chi0(:,:) = ZERO; vc(:,:)  = ZERO; xr(:,:) = ZERO
  ew(:)=0.d0; e(:)=0.d0
  !
  ! load eigenpotentials of atomic system and form the basis set
  ! dng is used as workspace
  !
  !OPEN( UNIT = stdout, FILE = './out_'//nd_nmbr, STATUS = 'UNKNOWN' )
  CALL real_to_char ( uu, string )
  !
  IF ( depdir1 == '' .and. depdir2 == '') THEN
     !
     call errore('eciu_tb','depdir1 and depdir2 are empty!!!',1)
     !
  ELSE IF ( depdir1 /= '' .and. depdir2 == '') THEN 
     !
     temp_dir_save = tmp_dir; tmp_dir = trimcheck( depdir1 )
     tmp_dir = TRIM( tmp_dir ) //"iu"// TRIM( string )//"/"
     if ( pot_collect ) then
        if (ionode) call diropn(iundvw,'dvr', 2*ngm_g, exst)
        call mp_bcast( exst, ionode_id, intra_image_comm ) ! NsC added the communicator... CHECK!!!!
     else
        call diropn(iundvw,'dvr', 2*ngm, exst)
     endif
     if (.not.exst) call errore('eciu_tb', 'freq. grids do not match', 1)

     call init_depot( half_nevl, depot1, -1, iundvw )
     depot2(:,:) = depot1(:,:)

     if ( pot_collect ) then
        if (ionode) close( unit = iundvw, status = 'keep' )
     else
        close( unit = iundvw, status = 'keep' )
     endif
     tmp_dir = temp_dir_save
     !
  ELSE IF ( depdir1 == '' .and. depdir2 /= '') THEN 
     !
     temp_dir_save = tmp_dir; tmp_dir = trimcheck( depdir2 )
     tmp_dir = TRIM( tmp_dir ) //"iu"// TRIM( string )//"/"
     if ( pot_collect ) then
        if (ionode) call diropn(iundvw,'dvr', 2*ngm_g, exst)
        call mp_bcast( exst, ionode_id, intra_image_comm ) ! NsC added the communicator... CHECK!!!!
     else
        call diropn(iundvw,'dvr', 2*ngm, exst)
     endif
     if (.not.exst) call errore('eciu_tb', 'freq. grids do not match', 1)

     call init_depot( half_nevl, depot2, -1, iundvw )
     depot1(:,:) = depot2(:,:)

     if ( pot_collect ) then
        if (ionode) close( unit = iundvw, status = 'keep' )
     else
        close( unit = iundvw, status = 'keep' )
     endif
     tmp_dir = temp_dir_save
     !
  ELSE
     ! 
     temp_dir_save = tmp_dir
     !
     tmp_dir = trimcheck( depdir1 )
     tmp_dir = TRIM( tmp_dir ) //"iu"// TRIM( string )//"/"
     if ( pot_collect ) then
        if (ionode) call diropn(iundvw,'dvr', 2*ngm_g, exst)
        call mp_bcast( exst, ionode_id, intra_image_comm ) ! NsC added the communicator... CHECK!!!!
     else
        call diropn(iundvw,'dvr', 2*ngm, exst)
     endif
     if (.not.exst) call errore('eciu_tb', 'freq. grids do not match', 1)

     call init_depot( half_nevl, depot1, -1, iundvw )

     if ( pot_collect ) then
        if (ionode) close( unit = iundvw, status = 'keep' )
     else
        close( unit = iundvw, status = 'keep' )
     endif
     !
     tmp_dir = trimcheck( depdir2 )
     tmp_dir = TRIM( tmp_dir ) //"iu"// TRIM( string )//"/"
     if ( pot_collect ) then
        if (ionode) call diropn(iundvw,'dvr', 2*ngm_g, exst)
        call mp_bcast( exst, ionode_id, intra_image_comm ) ! NsC added the communicator... CHECK!!!!
     else
        call diropn(iundvw,'dvr', 2*ngm, exst)
     endif
     if (.not.exst) call errore('eciu_tb', 'freq. grids do not match', 1)

     call init_depot( half_nevl, depot2, -1, iundvw )

     if ( pot_collect ) then
        if (ionode) close( unit = iundvw, status = 'keep' )
     else
        close( unit = iundvw, status = 'keep' )
     endif
     !
     tmp_dir = temp_dir_save
     !
  ENDIF 
  !
  write(stdout,'(7x,''Reading eigenpots done'')')
  !
  !  Form the basis set
  !
  DO i = 1, half_nevl
     m = 2 * i - 1
     ! shift eigenpots by vector tau1
     IF ( SQRT(SUM(tau1(:)**2)).GT.1.d-6 ) THEN
        r0(:) = tau1(:)/alat
        DO ig = 1, ngm
           arg = DDOT (3, g(1,ig), 1, r0(1), 1 ) * tpi
           depot1(ig,i) = depot1(ig,i)*CMPLX(cos(arg), -sin(arg), kind=DP)
        ENDDO
     ENDIF
     ! shift eigenpots by vector tau2
     IF ( SQRT(SUM(tau2(:)**2)).GT.1.d-6 ) THEN
        r0(:) = tau2(:)/alat
        DO ig = 1, ngm
           arg = DDOT (3, g(1,ig), 1, r0(1), 1 ) * tpi
           depot2(ig,i) = depot2(ig,i)*CMPLX(cos(arg), -sin(arg), kind=DP)
        ENDDO
     ENDIF
     !
     dvg(:,m  ) = depot1(:,i) + depot2(:,i)
     dvg(:,m+1) = depot1(:,i) - depot2(:,i)
     !
  END DO
  !
  DEALLOCATE( depot1, depot2 )
  !
  ! normalization
  !
  DO i = 1, nevl
     norm(i) = DDOT(2*ngm, dvg(1,i), 1, dvg(1,i), 1) 
  ENDDO
#if defined __MPI
  CALL mp_sum(norm (1:nevl),  intra_pool_comm)
#endif
  norm(1:nevl) = SQRT(norm(1:nevl))
  DO i = 1, nevl
     dvg(:,i) = dvg(:,i) / norm(i)
  ENDDO
  !
  ! compute matrix elements of sqrt(Vc)*Xo*sqrt(Vc)
  !
  IF ( lepsi .AND. lgamma ) epsi00 = 0.d0
  ALLOCATE ( dng(ngm,nevl) ) 
  dng(:,1:nevl) = dvg(:,1:nevl)
  IF ( lgamma .AND. lepsi .AND. (gstart==2) ) dng(1, 1:nevl) = (0.d0,0.d0)
  IF ( lepsi ) CALL vc12_dvg( ngm, nevl, xq, dng(1,1) )
  CALL drhoiu_by_dv( uu, ngm, nevl, dng(1,1) )
  IF ( lepsi ) CALL vc12_dvg( ngm, nevl, xq, dng(1,1) )
  ! special treatment for q+G=0 component
  IF ( lgamma .AND. lepsi .AND. (gstart==2) ) &
     dng(1, 1:nevl) = CMPLX(epsi00, 0.d0, kind=DP) * dvg(1, 1:nevl)
  !
  ! Compute matrix elements of Vc^{1/2}*Xo*Vc^{1/2}
  !
  CALL ZGEMM( 'C', 'N', nevl, nevl, ngm, ONE, dvg(1,1), ngm,&
               dng(1,1), ngm, ZERO, chi0(1,1), nevl )
#if defined __MPI
  CALL mp_sum( chi0(:,1:nevl), intra_pool_comm )
#endif
  !
  ! compute overlap matrix vc
  !
  CALL ZGEMM( 'C', 'N', nevl, nevl, ngm, ONE, dvg(1,1), ngm,&
              dvg(1,1), ngm, ZERO, vc(1,1), nevl )
#if defined __MPI
  CALL mp_sum( vc(:,1:nevl), intra_pool_comm )
#endif
  !
  ! arrange them in the right order
  !
  DO n = 1, nevl
     !
     vc  (n,n) = CMPLX( REAL(vc  (n,n)), 0.D0, kind=DP )
     chi0(n,n) = CMPLX( REAL(chi0(n,n)), 0.D0, kind=DP )
     !
     DO m = n + 1, nevl
        !
        IF ( lgamma ) THEN
           vc  (n,m) = CMPLX( REAL(vc  (n,m)), 0.D0, kind=DP )
           chi0(n,m) = CMPLX( REAL(chi0(n,m)), 0.D0, kind=DP )
           vc  (m,n) = CONJG( vc  (n,m) )
           chi0(m,n) = CONJG( chi0(n,m) )
        ELSE
           vc  (m,n) = CONJG( vc  (n,m) )
           chi0(m,n) = CONJG( chi0(n,m) )
        ENDIF
        !
     END DO
     !
  ENDDO
  CALL cdiaghg( nevl, nevl, chi0, vc, nevl, ew, xr )
  !
  e(1:nevl) = ew(1:nevl)
  !
  CALL stop_clock ('eciu_tb')
  !
  DEALLOCATE (dvg, dng, chi0, vc, ew, xr)
  !
  RETURN
  !
END SUBROUTINE eciu_tb
