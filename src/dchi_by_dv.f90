
! Copyright (C) 2001 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
#define ZERO ( 0.D0, 0.D0 )
#define ONE  ( 1.D0, 0.D0 )
!
!------------------------------------------------------------------
SUBROUTINE dchi_by_dv(iu_index, u, eig, ndim, m, dvg)
 !
 ! This routine computes results of applying Xo or X_rpa at given
 ! imaginary frequency iu to m perturbation potentials. Whether the
 ! calculation is for Xo or X_rpa is dertermined by the control
 ! variables in the routine solving linear systems. Input potentials
 ! dvg are in G-space and become induced densites on exit.
 !
 USE kinds,         ONLY : DP
 USE lsda_mod,      ONLY : nspin
 USE cell_base,     ONLY : omega
 USE wvfct,         ONLY : npwx, nbnd
 USE gvect,         ONLY : ngm, nl
 USE fft_base,      ONLY : dfftp
 USE fft_interfaces,ONLY : invfft
 USE mp_global,     ONLY : inter_pot_comm => inter_image_comm, npot => nimage
 USE mp,            ONLY : mp_sum
 USE control_acfdt, ONLY : dpsi_plus, dpsi_minus, summ_vc, summ_vc3, summ_vc12, &
                           dchi_iu, dchi_iu3, dchi_iu12, dvgenc
 !
 IMPLICIT NONE
 !
 ! I/O variables
 !
 INTEGER,  INTENT(IN) :: iu_index
 REAL(DP), INTENT(IN) :: u
 ! input: immagine frequency
 INTEGER, INTENT(IN) :: ndim, m
 ! input: dimension of the arrays for potential and charge density
 ! input: number of pot. 
 REAL(DP), INTENT(IN) :: eig(m)
 ! input: eigenvalues 
 COMPLEX(DP), INTENT(INOUT) :: dvg(ndim,m)
 ! input: purturbed potentials in G-space
 ! output: induced densities in G-space
 !
 ! local variables
 !
 LOGICAL,     ALLOCATABLE :: comp_dvc(:)
 COMPLEX(DP), ALLOCATABLE :: dchi0(:) 
 COMPLEX(DP), ALLOCATABLE :: dchi03(:) 
 COMPLEX(DP), ALLOCATABLE :: dchi012(:) 
 !
 COMPLEX(DP), ALLOCATABLE :: aux(:)
 ! auxiliary arrays for FFT
 INTEGER :: i 
 ! counter
 REAL(DP) :: fac 
 !
 ALLOCATE (comp_dvc(m))
 ALLOCATE (dpsi_plus (npwx,nbnd), dpsi_minus(npwx,nbnd))
 ALLOCATE (summ_vc(dfftp%nnr, nspin))
 ALLOCATE (summ_vc3(dfftp%nnr, nspin))
 ALLOCATE (summ_vc12(dfftp%nnr, nspin))
 ALLOCATE (dchi0  (dfftp%nnr))
 ALLOCATE (dchi03  (dfftp%nnr))
 ALLOCATE (dchi012  (dfftp%nnr))
 ALLOCATE (aux(dfftp%nnr))
 !
 ! various checks
 !
 IF ( ndim > ngm ) CALL errore('dchi_by_dv', 'ndim too large', abs(ndim))
 IF ( ndim <= 0  ) CALL errore('dchi_by_dv', 'ndim negative!', abs(ndim))
 !
 ! normalization factor
 fac = 1.0_DP/omega
 !
 ! Xo|dvg> or X|dvg> is computed in R-space
 !
 comp_dvc(:) = .TRUE. ! must be set to .TRUE. for the code work in case NPOT=1
 !
 ! Loop over potentials
 !
 dchi0(:) = ZERO
 dchi03(:) = ZERO
 dchi012(:) = ZERO
 !
 IF ( npot.GT.1 ) CALL distribute_dv ( m, comp_dvc )
 ! 
 DO i = 1, m
   !
   IF ( comp_dvc(i) ) THEN  
     ! 
     ! first bring dvg(:,i) to R-space
     aux(:) = ZERO; aux(nl(1:ndim)) = dvg(1:ndim,i)
     CALL invfft ('Dense', aux, dfftp)
     !
     ! solve the linear system to obtain the density response
     dvgenc(:,1,1) = aux(:); dvgenc(:,:,2) = ZERO
     IF (nspin==2) dvgenc(:,nspin,1) = dvgenc(:,1,1)
     !  
     ! solve the variational wcf. at given potential
     dpsi_plus (:,:) = ZERO;  dpsi_minus(:,:) = ZERO
     !
     CALL solve_linter_iu ( u )
     !
     ! here, we call a subroutine to do the detail calculations.  
     !
     summ_vc(:,:) = ZERO
     summ_vc3(:,:) = ZERO
     summ_vc12(:,:) = ZERO
     CALL solve_dec_dv ( u )
     !
     IF (nspin==2) summ_vc(:,nspin) = summ_vc(:,1) + summ_vc(:,nspin) 
     IF (nspin==2) summ_vc3(:,nspin) = summ_vc3(:,1) + summ_vc3(:,nspin) 
     IF (nspin==2) summ_vc12(:,nspin) = summ_vc12(:,1) + summ_vc12(:,nspin) 
     !
   ELSE
     !
     summ_vc(:, nspin) = ZERO 
     summ_vc3(:, nspin) = ZERO 
     summ_vc12(:, nspin) = ZERO 
     !
   ENDIF
   ! 
   dchi0(:)   = dchi0(:) + CMPLX(fac, 0.0_DP)*  summ_vc(:, nspin) * CMPLX((eig(i)/(eig(i)-1.0_DP)),0.D0, kind=DP) 
!   dchi0(:)   = dchi0(:) + CMPLX(fac, 0.0_DP)*  summ_vc(:, nspin) * CMPLX((eig(i)/(eig(i)-2.0_DP)),0.D0, kind=DP)  !!!RPAx for 2 el
   dchi03(:)  = dchi03(:) + CMPLX(fac, 0.0_DP)* summ_vc3(:, nspin) * CMPLX((eig(i)/(eig(i)-1.0_DP)),0.D0, kind=DP)
!   dchi03(:)  = dchi03(:) + CMPLX(fac, 0.0_DP)* summ_vc3(:, nspin) * CMPLX((eig(i)/(eig(i)-2.0_DP)),0.D0, kind=DP) !!!RPAx for 2 el
   dchi012(:) = dchi012(:) + CMPLX(fac, 0.0_DP)* summ_vc12(:, nspin) * CMPLX((eig(i)/(eig(i)-1.0_DP)),0.D0, kind=DP) 
!   dchi012(:) = dchi012(:) + CMPLX(fac, 0.0_DP)* summ_vc12(:, nspin) * CMPLX((eig(i)/(eig(i)-2.0_DP)),0.D0, kind=DP) !!!RPAx for 2 el
   !
 END DO
 !
#if defined __MPI
  ! mp_sum is used as a trick to circulate dchi0's
  IF ( npot.GT.1 ) CALL mp_sum ( dchi0(:), inter_pot_comm )
  IF ( npot.GT.1 ) CALL mp_sum ( dchi03(:), inter_pot_comm )
  IF ( npot.GT.1 ) CALL mp_sum ( dchi012(:), inter_pot_comm )
#endif
 !
 dchi_iu(:)=ZERO; dchi_iu(:) = dchi0(:)
 dchi_iu3(:)=ZERO; dchi_iu3(:) = dchi03(:)
 dchi_iu12(:)=ZERO; dchi_iu12(:) = dchi012(:)
 !
 !
 DEALLOCATE (comp_dvc)
 DEALLOCATE (dpsi_plus, dpsi_minus)
 DEALLOCATE ( aux       )
 DEALLOCATE ( dchi0     )
 DEALLOCATE ( dchi03    )
 DEALLOCATE ( dchi012   )
 DEALLOCATE ( summ_vc   )
 DEALLOCATE ( summ_vc3  )
 DEALLOCATE ( summ_vc12 )
 !
 RETURN
 !
END SUBROUTINE dchi_by_dv
