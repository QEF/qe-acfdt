!
! Copyright (C) 2001-2004 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!-----------------------------------------------------------------------
PROGRAM exx_acfdt
  !-----------------------------------------------------------------------
  !
  ! ... This is the main driver of the exx_acfdt code.
  ! ... It reads all the quantities calculated by pwscf.
  ! ... It computes:
  ! ... i)  LDA(GGA) exchange and exact exchange as well
  ! ... ii) LDA(GGA) correlation energy and the short range 
  ! ...     correction of RPA correlation energy
  !
  USE io_global,   ONLY : stdout
  USE mp_global,   ONLY : mp_startup, mp_global_end
  USE environment, ONLY : environment_start
  !
  IMPLICIT NONE
  !
  CHARACTER (LEN=9)   :: code = 'EXX'
  !
  ! Initialize MPI, clocks, print initial messages
  !
#if defined __MPI
  CALL mp_startup ( )
#endif
  CALL environment_start ( code )
  !
  WRITE( stdout, '(/5x,"Ultrasoft (Vanderbilt) Pseudopotentials")' )
  !
  ! ... and begin with the initialization part
  !
  CALL exx_readin()
  !
  CALL xc_rpa_plus ( )
  !
  CALL ks_exx ( )
  !
  WRITE( stdout, * )
  CALL print_clock ('EXX')
  WRITE( stdout, '(5X,"EXX routines")' )
  CALL print_clock( 'exx_grid' )
  CALL print_clock( 'exxinit' )
  CALL print_clock ('exxen2')
  CALL mp_global_end ()
#if defined (__MPI)
   WRITE( stdout, '(/,5X,"Parallel routines")' )
   !
   CALL print_clock( 'reduce' )
   CALL print_clock( 'fft_scatter' )
   CALL print_clock( 'ALLTOALL' )
#endif
  !
  STOP
  !
END PROGRAM exx_acfdt
!
!----------------------------------------------------------------------------
SUBROUTINE exx_readin()
  !----------------------------------------------------------------------------
  !
  !    This routine reads the control variables for the program exx_acfdt.x
  !    from standard input (unit 5).
  !    A second routine readfile reads the variables saved on a file
  !    by the self-consistent program.
  !
  USE kinds,             ONLY : DP
  USE io_global,         ONLY : ionode, ionode_id
  USE mp,                ONLY : mp_bcast
  USE run_info,          ONLY : title
  USE io_files,          ONLY : tmp_dir, prefix
  USE control_flags,     ONLY : twfcollect
  USE mp_global,         ONLY : nproc_pool, nproc_file, nproc_pool_file, intra_image_comm
  USE exx,               ONLY : nq1, nq2, nq3, exxdiv_treatment, x_gamma_extrapolation, ecutvcut, &
                                ecutfock
  USE gvecw,             ONLY : ecutwfc
  USE input_parameters,  ONLY : exx_fraction
  !
  IMPLICIT NONE
  !
  INTEGER :: ios
    ! integer variable for I/O control
  CHARACTER (LEN=256) :: outdir
  !
  CHARACTER(LEN=256), EXTERNAL :: trimcheck 
  !
  LOGICAL, EXTERNAL  :: imatches
  !
  !
  NAMELIST / INPUTEXX / outdir, prefix, nq1, nq2, nq3, exx_fraction, &
                        exxdiv_treatment, x_gamma_extrapolation, ecutvcut
  ! prefix       : the prefix of files produced by pwscf
  ! outdir       : directory where input, output, temporary files reside
  ! 
  IF (ionode) THEN
  !
  ! ... Input from file ?
  !
     CALL input_from_file ( )
  !
  ! ... Read the first line of the input file
  !
     READ( 5, '(A)', IOSTAT = ios ) title
  !
  ENDIF
  !
  CALL mp_bcast(ios, ionode_id, intra_image_comm ) ! NsC added the communicator... CHECK!!!!
  CALL errore( 'exx_reading', 'reading title ', ABS( ios ) )
  call mp_bcast ( title,       ionode_id, intra_image_comm ) ! NsC added the communicator... CHECK!!!
  !
  ! Rewind the input if the title is actually the beginning of inputph namelist
  IF( imatches("&inputexx", title)) THEN
    WRITE(*, '(6x,a)') "Title line not specified: using 'default'."
    title='default'
    REWIND(5, iostat=ios)
    CALL errore('exx_reading', 'Title line missing from input.', abs(ios))
  ENDIF
  !
  ! ... set default values for variables in namelist
  !
  CALL get_environment_variable( 'ESPRESSO_TMPDIR', outdir )
  IF ( TRIM( outdir ) == ' ' ) outdir = './'
  prefix       = 'pwscf'
  nq1          = 1
  nq2          = 1
  nq3          = 1
  exx_fraction = 1.d0
  exxdiv_treatment      = 'g-b'
  x_gamma_extrapolation = .TRUE.
  ecutvcut = 0.D0
  ! 
  ! ...  reading the namelist inputec
  !
  IF (ionode) READ( 5, INPUTEXX, IOSTAT = ios )
  !
  CALL mp_bcast(ios, ionode_id, intra_image_comm) !NsC added the communicator... CHECK!!!!
  !
  CALL errore( 'exx_readin', 'reading inputexx namelist', ABS( ios ) )
  !
  IF (ionode) tmp_dir = trimcheck (outdir)
  !
  ! ...  assign value for q point (this is to fool phq_init called later)
  !
  ! ... broadcasting all input variables to other nodes
  !
  CALL mp_bcast ( title,                 ionode_id, intra_image_comm )  ! NsC added the communicator... CHECK!!!!
  CALL mp_bcast ( tmp_dir,               ionode_id, intra_image_comm )
  CALL mp_bcast ( prefix,                ionode_id, intra_image_comm )
  CALL mp_bcast ( nq1,                   ionode_id, intra_image_comm )
  CALL mp_bcast ( nq2,                   ionode_id, intra_image_comm )
  CALL mp_bcast ( nq3,                   ionode_id, intra_image_comm )
  CALL mp_bcast ( exx_fraction,          ionode_id, intra_image_comm )
  CALL mp_bcast ( exxdiv_treatment,      ionode_id, intra_image_comm )
  CALL mp_bcast ( x_gamma_extrapolation, ionode_id, intra_image_comm )
  CALL mp_bcast ( ecutvcut,              ionode_id, intra_image_comm )
  !
  ! ... Check all namelist variables
  !
  !
  !   Here we finished the reading of the input file.
  !   Now allocate space for pwscf variables, read and check them.
  !
  !   amass will also be read from file:
  !   save its content in auxiliary variables
  !
  !
  ! read data produced by pwscf
  !
  CALL read_file ( )
  if (ecutfock <=0) ecutfock = 4._DP * ecutwfc
  !
  IF (nproc_pool /= nproc_pool_file .and. .not. twfcollect)  &
     CALL errore('exx_readin',&
     'pw.x run with a different number of pools. Use wf_collect=.true.',1)
  !
  !
  RETURN
  !
END SUBROUTINE exx_readin
