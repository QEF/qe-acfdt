
! Copyright (C) 2009 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
#define ZERO ( 0.D0, 0.D0 )
#define ONE  ( 1.D0, 0.D0 )
!
!------------------------------------------------------------------
SUBROUTINE vc_scf (iu_index, u, e, nevl, dv)
 !
 ! This routine computes the self consistency of Vc in ACFDT:
 ! Step 1: Read dvg from converged Davison diagonalization
 ! Step 2: call routine call routine() to start    
 ! for each m perturbation potentials at each input frequency u . 
 ! Potential are in G-space and unchange on exit
 !
 USE kinds,         ONLY : DP
 USE lsda_mod,      ONLY : nspin
 USE constants,     ONLY : fpi, e2
 USE cell_base,     ONLY : tpiba2
 USE qpoint,        ONLY : xq
 USE gvect,         ONLY : ngm, g, nl, gstart, nlm, ngm_g
 USE fft_base,      ONLY : dfftp
 USE fft_interfaces,ONLY : fwfft, invfft
 USE control_lr,    ONLY : lgamma
 USE control_acfdt, ONLY : control_vc_rpa, lepsi
 USE mp_global,     ONLY : nproc_pool, me_pool, root_pool, &
                                   inter_pool_comm, intra_pool_comm
 USE mp,                   ONLY : mp_bcast, mp_sum
 USE io_global,     ONLY : stdout, ionode, ionode_id
 !
 IMPLICIT NONE
 !
 ! I/O variables
 !
 INTEGER, INTENT(IN) :: iu_index
 ! index of imaginary frequency, u_ec(iu) = uu
 INTEGER, INTENT(IN) :: nevl
 ! input: number of eigenmodes to be computed
 !
 REAL(DP),INTENT(IN) :: u, e(nevl)
 ! input: immagine frequency
 ! input: eigenvalues
 ! 
 COMPLEX(DP), INTENT(IN) :: dv(ngm,nevl)
 ! local variables
 INTEGER :: i, ip, n,  nvecx
 !
 COMPLEX(DP), ALLOCATABLE :: gs(:)
 ! working place 

 COMPLEX(DP), ALLOCATABLE :: dvg(:,:), dng(:,:)
 ! potentials and density response in G-space

 COMPLEX(DP), ALLOCATABLE :: dvg1(:)
 ! pot. in G-space and work space for FFT in PARA

#if defined alloc_aux
  COMPLEX(DP), ALLOCATABLE :: aux(:)
#else
  COMPLEX(DP) :: aux(dfftp%nnr)
#endif
 INTEGER  :: ngvpp(0:nproc_pool)
 REAL(DP) :: norm
 LOGICAL  :: ortho_base = .true.
 REAL(DP), EXTERNAL :: DDOT 
 !
 nvecx = 4*nevl
 !
 ALLOCATE ( dvg(ngm,nevl), dng(ngm,nevl) ) 
 !
 ALLOCATE ( gs(nevl) )
#if defined alloc_aux
 ALLOCATE ( aux(dfftp%nnr) )
#endif
 !
 !
 CALL start_clock ('Vc_ACFDPT')
  !
  control_vc_rpa = .TRUE.
  !
  ! read converged eigenpots from previous iu
  ! call init_depot( neigv, dv, -1 )
  !
  dvg(:,:) = ZERO
  !
  dvg(:,1:nevl) = dv(:,1:nevl)
  !
  ! if q=(0,0,0) make sure that eigenpotentials are real in R-space
  !
  IF ( lgamma ) THEN
     !
     DO n = 1, nevl
        ! put dvg on the FFT grid
        aux(:) = ZERO; aux(nl(1:ngm)) = dvg(1:ngm,n)
        ! go to R-space
        CALL invfft ('Dense', aux, dfftp)
        ! keep only the real part
        aux(:) = CMPLX( DBLE(aux(:)), 0.d0, kind=DP )
        ! go back to G-space
        CALL fwfft ('Dense', aux, dfftp)
        ! put back to dvg
        dvg(1:ngm,n) = aux(nl(1:ngm))
     ENDDO
     !
  ENDIF
  !
  IF (ortho_base) THEN
     CALL start_clock ('ortho')
     DO i = 1, nevl
        CALL ZGEMM( 'C', 'N', i-1, 1, ngm, ONE, dvg, ngm, &
                    dvg(1,i), ngm, ZERO, gs(1), nvecx )
#if defined __MPI
        CALL mp_sum( gs(1:i-1), intra_pool_comm )
#endif
        CALL ZGEMM( 'N', 'N', ngm, 1, i-1, -ONE, dvg(1,1), ngm, &
                    gs(1), nvecx, ONE, dvg(1,i), ngm )
        ! and normalize
        norm = DDOT(2*ngm, dvg(1,i), 1, dvg(1,i), 1)
        !
#if defined __MPI
        CALL mp_sum( norm, intra_pool_comm )
#endif
        norm = 1.d0/SQRT(norm)
        CALL DSCAL (2*ngm, norm, dvg(1,i), 1 )
     ENDDO
     CALL stop_clock ('ortho')
  ENDIF
  !  
  dng(:,1:nevl) = dvg(:,1:nevl)
  IF ( lgamma .AND. lepsi .AND. (gstart==2) ) dng(1, 1:nevl) = (0.d0,0.d0)
  !
  IF ( lepsi ) CALL vc12_dvg( ngm, nevl, xq, dng(1,1) )
  !     
  CALL dchi_by_dv(iu_index, u, e, ngm, nevl, dng(1,1) )
  !
 DEALLOCATE (dvg, dng )
 DEALLOCATE (gs )
#if defined alloc_aux
  DEALLOCATE (aux)
#endif
 call stop_clock ('Vc_ACFDPT')
 RETURN
 !
END SUBROUTINE vc_scf
