!
! Copyright (C) 2001-2007 Quantum ESPRESSO group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
!
#define DEBUG
!----------------------------------------------------------------------------
SUBROUTINE add_exxrpa_vltot( potcorr)
  !----------------------------------------------------------------------------
  !
  ! ... This routine initializes the self consistent potential from given
  ! ... charge density and then add exxrpa potential to it (local or semilocal
  ! ... xc contribution must be set to zero)
  !
  USE kinds,                ONLY : DP
  USE io_global,            ONLY : stdout
  USE lsda_mod,             ONLY : nspin
  USE fft_base,             ONLY : dfftp
  USE cell_base,            ONLY : omega
  USE gvecs,                ONLY : doublegrid
  USE scf,                  ONLY : rho, rho_core, rhog_core, &
                                   vltot, v, vrs, kedtau
  USE ener,                 ONLY : ehart, etxc, vtxc, epaw
  USE ldaU,                 ONLY : eth
  USE control_acfdt,        ONLY : vx_exx, exxvx, vc_rpa, rpavc, &
                                   vc_init, vx_init, etx_init, vtx_init, &
                                   etc_init, vtc_init, ex_exx, ecrpa, mixpot_beta, &
                                   vtc_rpa, vtx_exx, delta_vxc, delta_vxc_old, delta_vtxc, delta_etxc
  USE io_pot_xml,           ONLY : write_pot
  USE mp_global,            ONLY : intra_pool_comm
  USE mp,                   ONLY : mp_sum

  !
  IMPLICIT NONE
  !
  LOGICAL, INTENT(IN) ::  potcorr
  !
  REAL(DP) :: charge           ! the starting charge
  REAL(DP) :: etotefield       !
  REAL(DP) :: aux(dfftp%nnr,nspin)
  !
  CALL start_clock('potinit')
  ! this routine calculate the correction to vxc to be addeded to vltot and
  ! the correction to etxc to be added to etot in the LDA-like scf cycle
  !
  delta_vxc_old(:)= delta_vxc(:)
  delta_vxc(:) = 0.0_DP
  delta_etxc   = 0.0_DP
  !
  IF (potcorr) CALL vxc_init( rho, rho_core, rhog_core, &
                    etx_init, vtx_init, etc_init, vtc_init, vx_init, vc_init )
  ! 
  IF ( vx_exx ) THEN
    ! add the exx-oep potential
    delta_vxc(:) = delta_vxc(:) + exxvx(:) 
    delta_etxc   = delta_etxc   + ex_exx
    IF (potcorr) delta_vxc(:) = delta_vxc(:) - vx_init(:,nspin)
#if defined DEBUG
    write(stdout,'("Add exxvx to local potential")')
    write(stdout,'(2F21.11)')minval(dble(exxvx(:))), maxval(dble(exxvx(:)))
#endif
  ENDIF
  !
  IF ( vc_rpa ) THEN
    ! add the rpa-oep potential
    delta_vxc(:) = delta_vxc(:) + rpavc(:) 
    delta_etxc   = delta_etxc   + ecrpa
    IF (potcorr) delta_vxc(:) = delta_vxc(:) - vc_init(:,nspin) 
#if defined DEBUG
    write(stdout,'("Add rpavc to local potential")')
    write(stdout,'(2F21.11)')minval(dble(rpavc(:))), maxval(dble(rpavc(:)))
#endif
  ENDIF
  !
  ! remove the LDA part correspoding to rho_0 
  !
  CALL v_of_rho( rho, rho_core, rhog_core, &
                 ehart, etxc, vtxc, eth, etotefield, charge, v )
  write(stdout,*)"charge density read in add_exxrpa_vrs"
  write(stdout,'(4F21.11)')rho%of_r(1:12,nspin)
  write(stdout,*)"v_of_r in add_exxrpa_vrs"
  write(stdout,'(4F21.11)')v%of_r(1:12,nspin)
  !
  IF(potcorr) delta_etxc = delta_etxc - etxc    
  !
  !  !!!!!!!!!!!!!!!! MIX delta_vxc if needed !!!!!!!!!!!!!!!
  !
  !!!! testing for this commen
  delta_vxc_old(:) = delta_vxc_old(:) - delta_vxc(:)
  !
  delta_vxc(:) = delta_vxc(:) !+ (1.0-mixpot_beta) * delta_vxc_old(:)
  ! 
  write(stdout,*)"delta_vxc MIX error"
  write(stdout,'(2F21.11)')minval(dble(delta_vxc_old(:))), maxval(dble(delta_vxc_old(:)))
  !
  !  !!!!!!!!!!!!!!!! end of MIXING here !!!!!!!!!!!!!!!!!!!!
  write(stdout,*)"delta_vxc in add_exxrpa_vrs"
  write(stdout,'(4F21.11)')delta_vxc(1:12)
  !
  ! save again the potential of exx/rpa for restart
  aux(:,:) = 0.0_DP; aux(:,nspin) = delta_vxc(:)
  CALL write_pot( aux, nspin)
  !!! test
  write(stdout,*) "the unchanged of exxrpa potential"
  write(stdout,*) aux(1:3,nspin)
  !!! test
  !
  ! compute correction to energy
  delta_vtxc =  omega * sum( delta_vxc(:)*rho%of_r(:,nspin) ) / (dfftp%nr1 * dfftp%nr2 * dfftp%nr3)
  CALL mp_sum( delta_vtxc, intra_pool_comm )
  delta_etxc = delta_etxc - delta_vtxc
  !
  ! re-initialize vltot
  !
  call setlocal()
  !
  ! ... compute the potential and store it in vr
  !
  write(stdout,*)"vltot in add_exxrpa_vrs"
  write(stdout,'(4F21.11)')vltot(1:12)
  !
  ! ... add delta_vxc term to vltot
  !
  vltot(:) = vltot(:) + delta_vxc(:)
  write(stdout,*)"vltot2 in add_exxrpa_vrs"
  write(stdout,'(4F21.11)')vltot(1:12)
  !
  ! ... define the total local potential (external+scf)
  !
  CALL set_vrs( vrs, vltot, v%of_r, kedtau, v%kin_r, dfftp%nnr, nspin, doublegrid )
  !CALL set_vrs_oep( vrs, vltot, v%of_r, kedtau, v%kin_r, dfftp%nnr, nspin, doublegrid )
  write(stdout,*)"vrs in add_exxrpa_vrs"
  write(stdout,'(4F21.11)')vrs(1:12,nspin)
  !
  CALL stop_clock('potinit')
  !
  RETURN
  !
END SUBROUTINE add_exxrpa_vltot
