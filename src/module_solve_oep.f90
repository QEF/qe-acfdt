
! Copyright (C) 2001 PWSCF group
! This file is distributed under the terms of the
! GNU General Public License. See the file `License'
! in the root directory of the present distribution,
! or http://www.gnu.org/copyleft/gpl.txt .
!
#define ZERO ( 0.D0, 0.D0 )
#define ONE  ( 1.D0, 0.D0 )
!
!==================================================================================
SUBROUTINE pot_pulay_mixing( pot_in, pot_out, dden_in, ddpot_in, betamix, oep_iter)
 !=================================================================================
 !
 USE kinds,         ONLY : DP
 USE io_global,     ONLY : stdout
 USE mp,            ONLY : mp_sum
 USE mp_global,     ONLY : intra_pool_comm
 USE lsda_mod,      ONLY : nspin
 USE gvect,         ONLY : ngm, nl
 USE fft_base,      ONLY : dfftp
 USE fft_interfaces,ONLY : fwfft, invfft
 USE acfdt_scf,     ONLY : oepdf_g, oeppot_g, oepvcdf_g, oepdpot_g
  !
  IMPLICIT NONE
  !
  ! I/O variables
  INTEGER,  INTENT(IN) :: oep_iter
  REAL(DP), INTENT(IN) :: betamix
  REAL(DP), INTENT(IN) :: pot_in (dfftp%nnr), dden_in (dfftp%nnr), ddpot_in(dfftp%nnr)
  REAL(DP), INTENT(INOUT) :: pot_out (dfftp%nnr)
  !
  !local variables
  !
  INTEGER     :: ndim, max_m, ik, i, j, k
  COMPLEX(DP) :: sum_tmp
  !
  REAL(DP), ALLOCATABLE :: dden_best(:), dpot_best(:), pot_best(:)
  COMPLEX(DP), ALLOCATABLE :: sum_dpot_g(:), sum_dden_g(:)
  !
  COMPLEX(DP), ALLOCATABLE :: aux(:), aux1(:)
  REAL(DP), ALLOCATABLE :: aux2(:)
  ! auxiliary arrays for FFT
  !
  INTEGER :: info
  REAL(DP):: alpha, lambda 
  REAL(DP),ALLOCATABLE :: alphamix(:,:), work(:)
  INTEGER, ALLOCATABLE :: iwork(:)
  !
  ndim = dfftp%nnr
  !
  ALLOCATE(aux(ngm), aux1(ndim), aux2(ndim))
    !
    !
  max_m = oep_iter        
    ik  = oep_iter
    !
    ! ... pass the pot_in to G-space
    !
    aux1(:)   = ZERO; aux1(:) = CMPLX(pot_in(:),0.D0, kind=DP)
    ! go back to G-space
    CALL fwfft ('Dense', aux1, dfftp)
    oeppot_g(:, ik ) = aux1(nl(:))
    !
    ! ... pass the ddpot_in to G-space
    !
    aux1(:)   = ZERO; aux1(:) = CMPLX(ddpot_in(:),0.D0, kind=DP)
    ! go back to G-space
    CALL fwfft ('Dense', aux1, dfftp)
    oepdpot_g(:, ik ) = aux1(nl(:))
    !
    ! ... pass the dden_i to G-space
    !
    aux1(:)   = ZERO; aux1(:) = CMPLX(dden_in(:),0.D0, kind=DP)
    ! go back to G-space
    CALL fwfft ('Dense', aux1, dfftp)
    oepdf_g(:, ik ) = aux1(nl(:))
    !
    sum_tmp = ZERO
    DO i = 1, ngm
      sum_tmp = sum_tmp + CONJG(oepdf_g(i,ik))*oepdf_g(i,ik)
    ENDDO
#if defined __MPI
          CALL mp_sum( sum_tmp, intra_pool_comm )
#endif
    write(stdout,*) "gradien norm", sqrt(dble(sum_tmp))
    !
    aux(:) = ZERO; aux(:) = aux1(nl(:)) 
    CALL vc_gorling(ngm, aux)
    !
    oepvcdf_g(:, ik ) = aux(:) 
    !
!!!!!!!!!!!!!!!!!!!!!!!
    sum_tmp = ZERO
    DO i = 1, ngm
      sum_tmp = sum_tmp + CONJG(oepdf_g(i,ik))*oepvcdf_g(i,ik)
    ENDDO
#if defined __MPI
          CALL mp_sum( sum_tmp, intra_pool_comm )
#endif

    write(stdout,*) "heheheheh", dble(sum_tmp)
!!!!!!!!!!!!!!!!!!!!!!!!  
    ! 
max_m = max_m + 1
    !
  ALLOCATE(alphamix(max_m, max_m)) !iter_used))
    !
    alphamix(:,:) = 0.0_DP
    DO i = 1, max_m 
      !  
      IF (i < max_m) THEN
        !
        DO j = i, (max_m-1)
          sum_tmp = ZERO
          DO k = 1, ngm
            sum_tmp = sum_tmp + CONJG(oepdf_g(k,i))*oepvcdf_g(k,j)
          ENDDO
#if defined __MPI
          CALL mp_sum( sum_tmp, intra_pool_comm )
#endif
          alphamix(i,j) = DBLE(sum_tmp)
          alphamix(j,i) = alphamix(i,j)
        write(stdout,*) i,j, alphamix(i,j)
        ENDDO
        !
      ELSE
        !
        DO j = 1, max_m
          IF (j == max_m) THEN
             alphamix(i,j) = 0.0_DP
          ELSE 
             alphamix(i,j) =-1.0_DP
             alphamix(j,i) =-1.0_DP
          ENDIF
        ENDDO  
        ! 
      ENDIF
      ! 
    ENDDO
    !
!!!!! Aij^-1 
    ! 
    ALLOCATE(work(max_m), iwork(max_m))
    CALL DSYTRF( 'U', max_m, alphamix , max_m, iwork, work, max_m, info )
    CALL errore( 'broyden', 'factorization', abs(info) )
    ! 
    CALL DSYTRI( 'U', max_m, alphamix , max_m, iwork, work, info )
    CALL errore( 'broyden', 'DSYTRI', abs(info) )    !
    DEALLOCATE(iwork)
    !
!!!!!
    !
    FORALL( i = 1:max_m, &
            j = 1:max_m, j > i ) alphamix(j,i) = alphamix(i,j)
    !
    DO i = 1, max_m 
    !
    work(i) = 0.0_DP
    IF (i == max_m) work(i) =-1.0_DP
    !
    ENDDO
    !
    ALLOCATE (sum_dpot_g(ngm), sum_dden_g(ngm), dpot_best(ndim), dden_best(ndim), pot_best(ndim))
    sum_dpot_g(:)  = ZERO
    sum_dden_g(:) = ZERO
    DO i = 1, max_m
      alpha = 0.0_DP
      DO j = 1, max_m
        alpha = alpha + (alphamix(i,j)* work(j) )
      ENDDO
      !
      write(stdout,*) alpha
      IF (i == max_m) THEN
        lambda = alpha   ! the lagrange multiplier 
      ELSE
        sum_dpot_g(:) = sum_dpot_g(:) + alpha * (oepdpot_g(:,i))
        sum_dden_g(:) = sum_dden_g(:) + alpha * (  oepdf_g(:,i))
      ENDIF
      !
    ENDDO
  DEALLOCATE(alphamix, work)
    ! 
300 CONTINUE
    !
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !put on the FFT grid 
    aux1(:) = ZERO; aux1(nl(:)) = sum_dpot_g(:)
    !go back to R-space 
    CALL invfft ('Dense', aux1, dfftp)
    !
    dpot_best(:) = DBLE(aux1(:))
    !
    !put on the FFT grid
    aux1(:) = ZERO; aux1(nl(:)) = sum_dden_g(:)
    !go back to R-space 
    CALL invfft ('Dense', aux1, dfftp)
    !
    dden_best(:) = DBLE(aux1(:))
    ! 
    !-----------new potential for output by simple mixing-----------!
    !
    pot_out(:) = 0.0_DP
    !if (oep_iter==1) then
    !    call solve_oep(ndim, dden_in, aux2 ) 
    ! pot_out(:) = pot_in(:) + aux2(:)
    ! pot_out(:) = pot_in(:) + ddpot_in(:)
    !else
    !   if (oep_iter<=4) then 
    !     pot_out(:) = pot_in(:) - dden_in(:) 
    !   else
    pot_out(:) = (1-betamix)*pot_in(:) + betamix* pot_best(:)
    !     pot_out(:) = pot_best(:) - dden_best(:)
    !   endif
    !endif     
    ! 
    !if (oep_iter==1) then
    !   pot_out(:) = pot_in(:) + ddpot_in(:)
    !else
    !   pot_out(:) = pot_in(:) + dpot_best(:)
    !endif
    !
    !--------new potential for output using inverted matrix---------!
    ! think about it !!!!!
    !call solve_oep(ndim, dden_best, aux2 ) 
    !
    !pot_out(:) = pot_best(:) + 0.1*aux2(:)
    !
    !
  DEALLOCATE ( aux, aux1, aux2)
  DEALLOCATE ( pot_best, dpot_best, dden_best, sum_dden_g, sum_dpot_g )
  !
  RETURN
  !
END SUBROUTINE pot_pulay_mixing
!============================================
!
!
!========================================================================
SUBROUTINE line_search_method( pot_in, pot_out, dden_in, ddpot_in, betamix, oep_iter)
 !=======================================================================
 !
 USE kinds,         ONLY : DP
 USE io_global,     ONLY : stdout
 USE mp_global,     ONLY : intra_pool_comm
 USE lsda_mod,      ONLY : nspin
 USE fft_base,      ONLY : dfftp
 USE acfdt_scf,     ONLY : line_search, poly_search, print_result, lambda_opt, &
                           line_search_index 
  !
  IMPLICIT NONE
  !
  ! I/O variables
  INTEGER,  INTENT(IN) :: oep_iter
  REAL(DP), INTENT(IN) :: betamix
  REAL(DP), INTENT(IN) :: pot_in  (dfftp%nnr, nspin)
  REAL(DP), INTENT(IN) :: ddpot_in(dfftp%nnr, nspin)
  COMPLEX(DP), INTENT(IN) :: dden_in (dfftp%nnr)
  REAL(DP), INTENT(INOUT) :: pot_out (dfftp%nnr, nspin)
  !
  !local variables
  !
  LOGICAL :: simple_line_search, quadratic_line_search, cubic_line_search
  REAL(DP):: lambda 
  REAL(DP), ALLOCATABLE :: dpot_best(:,:), pot_best(:,:)
  !
  !
  simple_line_search    = .false.
  quadratic_line_search = .false. 
  cubic_line_search = .false. 
  IF (line_search_index == 1) simple_line_search = .true.
  IF (line_search_index == 2) quadratic_line_search = .true.
  IF (line_search_index == 3) cubic_line_search = .true.
    !
    ALLOCATE (dpot_best(dfftp%nnr, nspin), pot_best(dfftp%nnr, nspin) )
    !
    !... line search methods is applyied here ...
    !
    pot_out(:,:) = 0.0_DP
    !  
    IF(simple_line_search) THEN
    ! !
    ! ! simple search, with lambda = 0.5
    ! !
      CALL line_search_lambda_linear(lambda, pot_in, ddpot_in, dden_in, pot_best, dpot_best)
      pot_out(:,:) = pot_best(:,:) + lambda * dpot_best(:,:)
      !
      lambda_opt   = lambda
      print_result = .true.  ! ... lambda here is optimized lambda, print exact results
    ! !
    ! ! quadratic search
    ! !
    ELSEIF(quadratic_line_search) THEN
      !
      IF (.not.line_search) THEN
         !
         CALL line_search_lambda_quadratic(.false., lambda, pot_in, ddpot_in, dden_in, pot_best, dpot_best)
         pot_out(:,:) = pot_best(:,:) + lambda * dpot_best(:,:)
         !pot_out(:) = pot_best(:) + lambda * dpot_best(:)
         !
         print_result = .false.   ! ... first guessed lambda, no print exact results
         !
      ELSE
         ! 
         CALL line_search_lambda_quadratic( .true., lambda, pot_in, ddpot_in, dden_in, pot_best, dpot_best)
         pot_out(:,:) = pot_best(:,:) + lambda * dpot_best(:,:)
         !pot_out(:) = pot_best(:) + lambda * dpot_best(:)
         !
         lambda_opt   = lambda  
         print_result = .true.    ! ... lambda here is optimized lambda, print exact results
         !
      ENDIF
    ! !
    ! ! cubic polynomial search
    ! ! 
    ELSEIF(cubic_line_search) THEN
      !       
      IF (.not.line_search) THEN
         !
         CALL line_search_lambda_cubic_polynomial(1, lambda, pot_in, ddpot_in, dden_in, pot_best, dpot_best)
         pot_out(:,:) = pot_best(:,:) + lambda * dpot_best(:,:)
         !pot_out(:) = pot_best(:) + lambda * dpot_best(:)
         !
         print_result = .false.   ! ... first guessed lambda, no print exact results
         !
      ELSE 
         IF (poly_search) THEN
           CALL line_search_lambda_cubic_polynomial (2, lambda, pot_in, ddpot_in, dden_in, pot_best, dpot_best)
           pot_out(:,:) = pot_best(:,:) + lambda * dpot_best(:,:)
           !pot_out(:) = pot_best(:) + lambda * dpot_best(:)
           !
           print_result = .false. ! ... second guessed lambda, no print exact results
           !  
         ELSE 
           CALL line_search_lambda_cubic_polynomial (3, lambda, pot_in, ddpot_in, dden_in, pot_best, dpot_best)
           pot_out(:,:) = pot_best(:,:) + lambda * dpot_best(:,:)
           !pot_out(:) = pot_best(:) + lambda * dpot_best(:)
           !
           lambda_opt   = lambda  
           print_result = .true.  ! ... lambda here is optimized lambda, print exact results
           ! 
         ENDIF
      ENDIF
      write(stdout,*) "step_lambda", lambda
      write(stdout,'(2F21.11)') minval(dble(dpot_best(:,:))), maxval(dble(dpot_best(:,:))) 
      !
    ELSE
       !
       CALL errore ('line_search_method', 'Wrong line_search_index', 1)
       ! 
    ENDIF
    !
  DEALLOCATE ( pot_best, dpot_best )
  !
  RETURN
  !
END SUBROUTINE line_search_method
!=================================
!
!
!====================================================================================================
SUBROUTINE line_search_lambda_linear (lambda, pot_in, ddpot_in, dden_in, pot_out, dpot_out)
  !==================================================================================================
  !
  USE kinds,         ONLY : DP
  USE fft_base,      ONLY : dfftp
  USE lsda_mod,      ONLY : nspin
  USE xml_io_base,   ONLY : write_rho
  USE acfdt_scf,     ONLY : save_norm_1, line_search
  USE io_files,      ONLY : prefix, tmp_dir
  !
  IMPLICIT NONE
  !
  ! I/O variables
  !
  REAL(DP), INTENT(INOUT) :: lambda
  REAL(DP),    INTENT(IN) :: pot_in(dfftp%nnr, nspin),  ddpot_in(dfftp%nnr, nspin)
  COMPLEX(DP), INTENT(IN) :: dden_in(dfftp%nnr)
  REAL(DP), INTENT(INOUT) :: pot_out(dfftp%nnr, nspin), dpot_out(dfftp%nnr, nspin)
  !  
  !local variables
  !
  REAL(DP) :: aux(dfftp%nnr, nspin)
  REAL(DP) :: save_pot(dfftp%nnr, nspin)
  REAL(DP) :: save_ddpot(dfftp%nnr, nspin)
  CHARACTER(LEN=256) :: dirname
  !
  dirname = TRIM(tmp_dir) // TRIM(prefix) // '.save/'
  !
  lambda = 0.5_DP
  !
  ! 1) save again the 
  !
  save_pot(:,:) = 0.0_DP; save_pot(:,:) = pot_in(:,:)
  !    
  save_ddpot(:,:) = 0.0_DP; save_ddpot(:,:) = ddpot_in(:,:)
  !
  ! ... save on disk
  !
  aux(:,:) = 0.0_DP; aux(:,:) = save_pot(:,:)
  CALL write_rho(dirname, aux, nspin, 'potential_pot_line_search')
  !
  aux(:,:) = 0.0_DP; aux(:,:) = save_ddpot(:,:)
  CALL write_rho(dirname, aux, nspin, 'potential_ddpot_line_search')
  !
  ! 2) return the variables 
  !
  pot_out (:,:) = save_pot(:,:)
  !
  dpot_out(:,:) = save_ddpot(:,:)
  !
  ! ... deactivate line search method
  !
  line_search = .false.
  !
  ! ... deactivate line search information
  !
  save_norm_1 = 0.0_DP
  !
  RETURN
  !
END SUBROUTINE line_search_lambda_linear
!=================================
!
!
!====================================================================================================
SUBROUTINE line_search_lambda_quadratic (iflag, lambda, pot_in, ddpot_in, dden_in, pot_out, dpot_out)
  !==================================================================================================
  !
  USE kinds,         ONLY : DP
  USE io_global,     ONLY : stdout
  USE cell_base,     ONLY : omega
  USE mp,            ONLY : mp_sum
  USE mp_global,     ONLY : intra_pool_comm
  USE fft_base,      ONLY : dfftp
  USE lsda_mod,      ONLY : nspin
  USE xml_io_base,   ONLY : write_rho, read_rho
  USE acfdt_scf,     ONLY : save_norm_1, line_search 
  USE io_files,      ONLY : tmp_dir, prefix
  !
  IMPLICIT NONE
  !
  ! I/O variables
  !
  LOGICAL,  INTENT(IN) :: iflag
  REAL(DP), INTENT(INOUT) :: lambda
  REAL(DP), INTENT(IN) :: pot_in(dfftp%nnr, nspin), ddpot_in(dfftp%nnr, nspin)
  COMPLEX(DP), INTENT(IN) :: dden_in(dfftp%nnr)
  REAL(DP), INTENT(INOUT) :: pot_out(dfftp%nnr, nspin), dpot_out(dfftp%nnr, nspin)
  !
  !local variables
  !
  INTEGER  :: i, is, ndim
  REAL(DP) :: sum_tmp, norm
  REAL(DP) :: aux(dfftp%nnr, nspin)
  REAL(DP) :: save_pot(dfftp%nnr, nspin)
  REAL(DP) :: save_ddpot(dfftp%nnr, nspin)
  CHARACTER(LEN=256) :: dirname
  !
  ! auxiliary variables
  !
  !
  ndim = dfftp%nnr
  dirname = TRIM(tmp_dir) // TRIM(prefix) // '.save/'
  !
  IF(.not.iflag) THEN
    !
    ! ... line search method has not been activated yet
    !
    ! 1) estimate <dE/dV| dV> at lambda = 0   
    !
    sum_tmp = 0.0_DP
    DO i = 1, ndim
       !  
       DO is = 1, nspin
          sum_tmp = sum_tmp + (CONJG(dden_in(i)) * CMPLX(ddpot_in(i, is) , 0.D0, kind=DP) )
       ENDDO
       !
    ENDDO
    sum_tmp = omega * sum_tmp / (dfftp%nr1 * dfftp%nr2 * dfftp%nr3) 
#if defined __MPI
    CALL mp_sum( sum_tmp, intra_pool_comm )
#endif
    !
    norm = sum_tmp
    !
    ! 2) save again the 
    !
    save_norm_1 = norm 
    ! 
    save_pot(:,:) = 0.0_DP; save_pot(:,:) = pot_in(:,:)
    !    
    save_ddpot(:,:) = 0.0_DP; save_ddpot(:,:) = ddpot_in(:,:)
    !
    ! ... save on disk
    !
    aux(:,:) = 0.0_DP; aux(:,:) = save_pot(:,:)
    CALL write_rho(dirname, aux, nspin, 'potential_pot_line_search')
    !
    aux(:,:) = 0.0_DP; aux(:,:) = save_ddpot(:,:)
    CALL write_rho(dirname, aux, nspin, 'potential_ddpot_line_search')
    !
    !  3) return the variables 
    !
    pot_out(:,:)  = 0.0_DP; pot_out (:,:) = pot_in(:,:)
    !
    dpot_out(:,:) = 0.0_DP; dpot_out(:,:) = ddpot_in(:,:)
    !
    lambda = 0.5_DP
    !
    ! ... activate line search method 
    !
    line_search = .true. 
    !
  ELSE
    !
    ! ... line search method has been activated 
    !
    aux(:,:) = 0.0_DP
    CALL read_rho(dirname, aux, nspin, 'potential_pot_line_search')
    save_pot(:,:) = 0.0_DP; save_pot(:,:) = aux(:,:) 
    !
    aux(:,:) = 0.0_DP
    CALL read_rho(dirname, aux, nspin, 'potential_ddpot_line_search')
    save_ddpot(:,:) = 0.0_DP; save_ddpot(:,:) = aux(:,:)
    !
    ! 1) estimate <dE/dV| dV> at lambda = 1  
    !
    sum_tmp = 0.0_DP
    DO i = 1, ndim
       !
       DO is = 1, nspin
         sum_tmp = sum_tmp + (CONJG(dden_in(i)) * CMPLX(save_ddpot(i, is), 0.D0, kind=DP))
       ENDDO
       !
    ENDDO
    sum_tmp = omega * sum_tmp / (dfftp%nr1 * dfftp%nr2 * dfftp%nr3) 
#if defined __MPI
    CALL mp_sum( sum_tmp, intra_pool_comm )
#endif
    !
    norm = sum_tmp  
    ! 
    ! 2) return the variables 
    !
    pot_out (:,:) = save_pot(:,:)
    !
    dpot_out(:,:) = save_ddpot(:,:)
    !
    lambda = 0.5_DP*save_norm_1/(save_norm_1-norm)   
    !
    write(stdout,*) "a_and_b", (norm-save_norm_1)/(2*0.5), save_norm_1
    write(stdout,*) "opt_lamda", lambda
    !
    ! ... deactivate line search method
    !
    line_search = .false.
    !
    ! ... deactivate line search information
    !
    save_norm_1 = 0.0_DP
    !
  ENDIF
  !
  RETURN
  !
END SUBROUTINE line_search_lambda_quadratic
!============================================
!
!
!=============================================================================================================
SUBROUTINE line_search_lambda_cubic_polynomial (iflag, lambda, pot_in, ddpot_in, dden_in, pot_out, dpot_out)
  !===========================================================================================================
  !
  USE kinds,         ONLY : DP
  USE io_global,     ONLY : stdout
  USE cell_base,     ONLY : omega
  USE mp,            ONLY : mp_sum
  USE mp_global,     ONLY : intra_pool_comm
  USE fft_base,      ONLY : dfftp
  USE lsda_mod,      ONLY : nspin
  USE xml_io_base,   ONLY : write_rho, read_rho
  USE acfdt_scf,     ONLY : save_norm, save_norm_1, save_norm_2, &
                            line_search, poly_search
  USE io_files,      ONLY : tmp_dir, prefix
  !
  IMPLICIT NONE
  !
  ! I/O variables
  !
  INTEGER,  INTENT(IN) :: iflag
  REAL(DP), INTENT(INOUT) :: lambda
  REAL(DP), INTENT(IN) :: pot_in(dfftp%nnr, nspin), ddpot_in(dfftp%nnr, nspin)
  COMPLEX(DP), INTENT(IN) :: dden_in(dfftp%nnr)
  REAL(DP), INTENT(INOUT) :: pot_out(dfftp%nnr, nspin), dpot_out(dfftp%nnr, nspin)
  !
  !local variables
  !
  INTEGER  :: i, is, ndim
  REAL(DP) :: sum_tmp, norm
  REAL(DP) :: g1, g2, g3           ! derivatives of energy wrt lamda
  REAL(DP) :: lambda_opt           ! optimized lambda
  !
  REAL(DP) :: aux(dfftp%nnr, nspin) 
  REAL(DP) :: save_pot(dfftp%nnr, nspin)
  REAL(DP) :: save_ddpot(dfftp%nnr, nspin)
  CHARACTER(LEN=256) :: dirname
  !
  ndim = dfftp%nnr
  dirname = TRIM(tmp_dir) // TRIM(prefix) // '.save/'
  !
  IF(iflag == 1) THEN
    !
    ! ... line search method has not been activated yet
    !
    ! 1) estimate <dE/dV| dV> at lambda = 0   
    !
    sum_tmp = 0.0_DP
    DO i = 1, ndim
       !
       DO is = 1, nspin
          sum_tmp = sum_tmp + (CONJG(dden_in(i)) * CMPLX(ddpot_in(i, is), 0.D0, kind=DP) )
       ENDDO
       !
    ENDDO
    sum_tmp = omega * sum_tmp / (dfftp%nr1 * dfftp%nr2 * dfftp%nr3) 
#if defined __MPI
    CALL mp_sum( sum_tmp, intra_pool_comm )
#endif
    !
    norm = sum_tmp
    !
    ! 2) save again the 
    !
    save_norm_1 = norm         ! g1
    ! 
    save_pot(:,:) = 0.0_DP;   save_pot(:,:) = pot_in(:,:)
    !    
    save_ddpot(:,:) = 0.0_DP; save_ddpot(:,:) = ddpot_in(:,:)
    !
    ! ... save on disk
    !
    aux(:,:) = 0.0_DP; aux(:,:) = save_pot(:,:) 
    CALL write_rho(dirname, aux, nspin, 'potential_pot_line_search')
    !
    aux(:,:) = 0.0_DP; aux(:,:) = save_ddpot(:,:) 
    CALL write_rho(dirname, aux, nspin, 'potential_ddpot_line_search')
    !
    !  3) return the variables 
    !
    pot_out(:,:)  = 0.0_DP; pot_out (:,:) = pot_in(:,:)
    !
    dpot_out(:,:) = 0.0_DP; dpot_out(:,:) = ddpot_in(:,:)
    !
    lambda = 1.0_DP
    !
    ! ... activate line search method 
    !
    line_search = .true.
    !
    poly_search = .true.
    !
  ELSEIF(iflag == 2) THEN
    !
    ! ... line search method has been activated 
    !
    aux(:,:) = 0.0_DP
    CALL read_rho(dirname, aux, nspin, 'potential_pot_line_search')
    save_pot(:,:) = 0.0_DP; save_pot(:,:) = aux(:,:)
    !
    aux(:,:) = 0.0_DP
    CALL read_rho(dirname, aux, nspin, 'potential_ddpot_line_search')
    save_ddpot(:,:) = 0.0_DP; save_ddpot(:,:) = aux(:,:)
    !
    !
    ! 1) estimate <dE/dV| dV> at lambda = 1  
    !
    sum_tmp = 0.0_DP
    DO i = 1, ndim
       !
       DO is = 1, nspin
         sum_tmp = sum_tmp + (CONJG(dden_in(i)) * CMPLX(save_ddpot(i, is), 0.D0, kind=DP))
       ENDDO
       ! 
    ENDDO
    sum_tmp = omega * sum_tmp / (dfftp%nr1 * dfftp%nr2 * dfftp%nr3) 
#if defined __MPI
    CALL mp_sum( sum_tmp, intra_pool_comm )
#endif
    !
    norm = sum_tmp
    !
    ! 2) save again the 
    !
    save_norm_2 = norm          ! g2
 
    ! 2) return the variables 
    !
    pot_out (:,:) = save_pot(:,:)
    !
    dpot_out(:,:) = save_ddpot(:,:)
    !
    lambda = 0.5_DP
    !
    ! ... still activate line search method
    !
    line_search = .true.
    !
    poly_search   = .false. 
    !
   ELSE
    !
    ! ... line search has still been activated 
    !
    aux(:,:) = 0.0_DP
    CALL read_rho(dirname, aux, nspin, 'potential_pot_line_search')
    save_pot(:,:) = 0.0_DP; save_pot(:,:) = aux(:,:)
    !
    aux(:,:) = 0.0_DP
    CALL read_rho(dirname, aux, nspin, 'potential_ddpot_line_search')
    save_ddpot(:,:) = 0.0_DP; save_ddpot(:,:) = aux(:,:)
    !
    ! 1) estimate <dE/dV| dV> at lambda = 0.5  
    !
    sum_tmp = 0.0_DP
    DO i = 1, ndim
       !
       DO is = 1, nspin
         sum_tmp = sum_tmp + (CONJG(dden_in(i)) * CMPLX(save_ddpot(i, is), 0.D0, kind=DP))
       ENDDO
       !
    ENDDO
    sum_tmp = omega * sum_tmp / (dfftp%nr1 * dfftp%nr2 * dfftp%nr3) 
#if defined __MPI
    CALL mp_sum( sum_tmp, intra_pool_comm )
#endif
    !
    norm = sum_tmp    ! g3
    !
    ! 3) read the saved derivatives
    !
    g1  =  save_norm_1
    g2  =  save_norm_2
    g3  =  sum_tmp        
    !
    ! 3) return the variables 
    !
    pot_out (:,:) = save_pot(:,:)
    !
    dpot_out(:,:) = save_ddpot(:,:)
    !
    ! 4) estimate optimized lambda 
    !
    CALL cubic_polynomial(g1, g2, g3, lambda_opt )    
    !
    ! 5) return lambda 
    !
    lambda = lambda_opt
    !
    ! ... deactivate line search method
    !
    line_search = .false.
    !
    ! ... deactivate line search information
    !
    save_norm_1 = 0.0_DP; save_norm_2 = 0.0_DP
    !
  ENDIF
  !
  RETURN
  !
END SUBROUTINE line_search_lambda_cubic_polynomial
!===================================================
!
!
!================================================
SUBROUTINE cubic_polynomial (g1, g2, g3, lambda_opt )
  !==============================================
  ! 
  !  f(x) =  ax^3 +  bx^2 + cx + d 
  ! df(x) = 3ax^2 + 2bx   + c
  ! df2(x)= 6ax   + 2b
  !
  USE kinds,         ONLY : DP
  USE io_global,     ONLY : stdout
  ! 
  IMPLICIT NONE
  ! 
  ! I/O variables
  ! 
  REAL(DP), INTENT(IN)    :: g1, g2, g3
  REAL(DP), INTENT(INOUT) :: lambda_opt
  ! 
  !local variables
  ! 
  REAL(DP) :: a, b, c     ! cubic polynomial coffecients 
  REAL(DP) :: lambda_1, lambda_2   ! minima of cubic polynomial
  REAL(DP) :: df2_lambda1, df2_lambda2 ! second derivative at minima lambda
  REAL(DP) :: delta
  !
  a = (2.0_DP*g2 - 4.0_DP*g3 + 2.0_DP*g1)/3.0_DP  
  b = (4.0_DP*g3 -        g2 - 3.0_DP*g1)/2.0_DP
  c = g1
  !
  delta = b*b - 3*a*c
  write(stdout,*) "check_local_minia", b*b - 3*a*c
  !
  ! checking for delta if it gives result or not ... 
  !
  IF (delta < 0 ) THEN     ! cubic polynomial function have no solution
    IF (a < 0 ) THEN
       lambda_opt = -1.0_DP*(g1/(g2-g1)) ! here is an approximation of quadratic solution 
    ELSE
       lambda_opt = 0.5    ! here is just an approximation 
    ENDIF
    !
    RETURN
    !
  ENDIF
  !  
  lambda_1 = (-1.0_DP*b + (b*b - 3.0_DP*a*c)**0.5_DP )/(3.0_DP*a)
  lambda_2 = (-1.0_DP*b - (b*b - 3.0_DP*a*c)**0.5_DP )/(3.0_DP*a)
  !
  df2_lambda1 = 6.0_DP*a*lambda_1 + 2.0_DP*b 
  df2_lambda2 = 6.0_DP*a*lambda_2 + 2.0_DP*b  
  !
  write(stdout,*) "two solutions of lambda", df2_lambda1, df2_lambda2
  IF(df2_lambda1 > 0) THEN 
    lambda_opt = lambda_1
  ELSE
    lambda_opt = lambda_2
  ENDIF
  !
  RETURN
  !
END SUBROUTINE cubic_polynomial 
!==============================
